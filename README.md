The extractor performs 3 stages

- Parse: This stage parses each of the pdfs individually.

    This is a CPU heavy task.
    By increasing the memory limits on the regexes, the performance is not an issue.

    The use of rayon increases performance, but marginally (1 core runs in 11 seconds, 4 cores run in 8 seconds).
    So, the parsing is just done on a single thread.

- Unify: This stage combines the individually parsed pdfs into a single timetable
- Merge: This stage takes two timetables, `current` and `new`, and creates a new timetable
    with both sets of objects where `new` may overwrite `current`. This is useful to ensure stable ids

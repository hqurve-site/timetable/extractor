//! This module specify the timetable types

use std::collections::{BTreeMap, BTreeSet};

use chrono::NaiveTime;
use serde::{Deserialize, Serialize};

pub type Timestamp = i64;
pub type Id = u64;
pub type Day = usize;
pub type Week = usize;

#[serde_with::serde_as]
#[derive(Clone, Hash, PartialEq, Eq, Debug, Serialize, Deserialize, PartialOrd, Ord, Default)]
pub struct Timeperiod {
    #[serde_as(as = "crate::serde_help::SerdeHelp")]
    pub start: NaiveTime,
    #[serde_as(as = "crate::serde_help::SerdeHelp")]
    pub end: NaiveTime,
}

#[derive(Clone, Hash, PartialEq, Eq, Debug, Serialize, Deserialize, Default)]
pub struct Session {
    pub id: Id,
    pub last_modified_timestamp: Timestamp,
    pub enabled: bool,

    pub r#type: String,
    pub day: Day,
    pub timeperiod: Timeperiod,
    pub containers: BTreeSet<Id>,

    pub weeks: Vec<Week>,
    pub tags: BTreeMap<String, Vec<String>>,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Default)]
pub struct Container {
    pub id: Id,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub last_modified_timestamp: Option<Timestamp>,
    pub enabled: bool,

    pub r#type: String,
    pub code: String,
    pub name: Option<String>,
    pub sessions: BTreeSet<Id>,
}

#[derive(Debug, Default, Serialize, Deserialize, PartialEq, Clone)]
pub struct Timetable {
    pub containers: Vec<Container>,
    pub sessions: Vec<Session>,
}

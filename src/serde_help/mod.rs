use chrono::{NaiveDateTime, NaiveTime};
use regex::{Regex, RegexBuilder};
use serde::{Deserialize, Serialize};

use serde_with::{DeserializeAs, SerializeAs};

pub struct SerdeHelp;

impl SerializeAs<Regex> for SerdeHelp {
    fn serialize_as<S>(source: &Regex, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        source.as_str().serialize(serializer)
    }
}
impl<'de> DeserializeAs<'de, Regex> for SerdeHelp {
    fn deserialize_as<D>(deserializer: D) -> Result<Regex, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        RegexBuilder::new(&s)
            .size_limit(10 * (1 << 24))
            .build()
            .map_err(serde::de::Error::custom)
    }
}

const NAIVE_TIME_FORMAT: &str = "%H:%M:%S";
impl SerializeAs<NaiveTime> for SerdeHelp {
    fn serialize_as<S>(source: &NaiveTime, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        source
            .format(NAIVE_TIME_FORMAT)
            .to_string()
            .serialize(serializer)
    }
}
impl<'de> DeserializeAs<'de, NaiveTime> for SerdeHelp {
    fn deserialize_as<D>(deserializer: D) -> Result<NaiveTime, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        NaiveTime::parse_from_str(
            String::deserialize(deserializer)?.as_str(),
            NAIVE_TIME_FORMAT,
        )
        .map_err(serde::de::Error::custom)
    }
}

// const NAIVE_DATE_FORMAT: &str = "%Y-%m-%d";
// impl SerializeAs<NaiveDate> for SerdeHelp {
//     fn serialize_as<S>(source: &NaiveDate, serializer: S) -> Result<S::Ok, S::Error>
//     where
//         S: serde::Serializer {
//             source.format(NAIVE_DATE_FORMAT).to_string().serialize(serializer)
//     }
// }
// impl <'de> DeserializeAs<'de, NaiveDate> for SerdeHelp {
//     fn deserialize_as<D>(deserializer: D) -> Result<NaiveDate, D::Error>
//     where
//         D: serde::Deserializer<'de> {
//             NaiveDate::parse_from_str(String::deserialize(deserializer)?.as_str(), NAIVE_DATE_FORMAT)
//                 .map_err(serde::de::Error::custom)
//    }
// }

const NAIVE_DATE_TIME_FORMAT: &str = "%Y-%m-%dT%H:%M:%S";
impl SerializeAs<NaiveDateTime> for SerdeHelp {
    fn serialize_as<S>(source: &NaiveDateTime, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        source
            .format(NAIVE_DATE_TIME_FORMAT)
            .to_string()
            .serialize(serializer)
    }
}
impl<'de> DeserializeAs<'de, NaiveDateTime> for SerdeHelp {
    fn deserialize_as<D>(deserializer: D) -> Result<NaiveDateTime, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        NaiveDateTime::parse_from_str(
            String::deserialize(deserializer)?.as_str(),
            NAIVE_DATE_TIME_FORMAT,
        )
        .map_err(serde::de::Error::custom)
    }
}

impl SerializeAs<&'static encoding_rs::Encoding> for SerdeHelp {
    fn serialize_as<S>(
        source: &&'static encoding_rs::Encoding,
        serializer: S,
    ) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        source.name().to_string().serialize(serializer)
    }
}
impl<'de> DeserializeAs<'de, &'static encoding_rs::Encoding> for SerdeHelp {
    fn deserialize_as<D>(deserializer: D) -> Result<&'static encoding_rs::Encoding, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let label = String::deserialize(deserializer)?;
        encoding_rs::Encoding::for_label(label.as_bytes())
            .ok_or_else(|| serde::de::Error::custom(format!("bad encoding scheme \"{}\"", label)))
    }
}

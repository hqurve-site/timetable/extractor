mod download;
mod files;
mod merge;
mod parse;
mod serde_help;
mod timetable;
mod unify;

use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;

use clap::{Parser, Subcommand};

use anyhow::{Context, Result};

#[derive(Parser)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Parses all of the pdfs in parallel
    ParseParallel,
    /// Combines all the parsed pdfs into one
    UnifyTimetable,
    /// Merges a list of timetables into one
    MergeTimetables,

    // Debug functions
    /// Parses one single pdf
    ParseSingle {
        /// Save the outputs for each stage and page
        #[arg(long)]
        debug: bool,

        resource_id: String,

        /// Alternative config location
        config_location: Option<String>,
    },
}

fn main() -> Result<()> {
    let indicatif_layer = tracing_indicatif::IndicatifLayer::new();
    let filter = tracing_subscriber::EnvFilter::builder()
        .with_default_directive(tracing_subscriber::filter::LevelFilter::INFO.into())
        .from_env()
        .context("building env_filter")?;
    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer().with_writer(indicatif_layer.get_stderr_writer()))
        .with(indicatif_layer)
        .with(filter)
        .init();

    let cli = Cli::parse();

    match cli.command {
        Commands::ParseParallel => {
            let _span = tracing::info_span!("parse_parallel").entered();

            let config =
                files::deserialize_sync(files::PARSE_CONFIG).context("loading parse config")?;
            tracing::info!("loaded config");

            parse::run(&config).context("running parse")?;
        }
        Commands::UnifyTimetable => {
            let _span = tracing::info_span!("unify_timetable").entered();
            let config =
                files::deserialize_sync(files::UNIFY_CONFIG).context("loading unify config")?;
            tracing::info!("loaded config");

            unify::run_unify(&config).context("running unify")?;
        }
        Commands::MergeTimetables => {
            let _span = tracing::info_span!("merge_timetable").entered();
            let config =
                files::deserialize_sync(files::MERGE_CONFIG).context("loading merge config")?;
            tracing::info!("loaded config");

            merge::run(&config).context("running merge")?;
        }
        Commands::ParseSingle {
            resource_id,
            debug,
            config_location,
        } => {
            let _span = tracing::info_span!("parse_single").entered();
            let config =
                files::deserialize_sync(config_location.as_deref().unwrap_or(files::PARSE_CONFIG))
                    .context("loading parse config")?;
            tracing::info!("loaded config");

            parse::run_single(&resource_id, &config, debug).context("running parse_single")?;
        }
    }

    Ok(())
}

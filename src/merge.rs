//! This file takes two timetables: current and new and merges new into current.
//! This is done to ensure that ids can be preserved, even when data is updated.
//! General Rules:
//! - if present in both, keep and update current
//! - if not present in current but present in new, add new to current and assign new id
//! - if present in current but not present in new, disable current

use std::collections::{BTreeSet, HashMap, HashSet};

use anyhow::{Context, Result};
use itertools::Itertools;
use serde::Deserialize;

use crate::{
    files,
    timetable::{Container, Id, Session, Timetable},
};

#[derive(Debug, Deserialize)]
pub struct Config {
    /// list of paths of timetables to merge
    pub list_filenames: Vec<String>,

    /// output filename
    pub output_filename: String,
}

pub fn run(config: &Config) -> Result<()> {
    let mut list = Vec::new();
    for filename in &config.list_filenames {
        let timetable = files::deserialize_sync(filename).context("loading input timetable")?;
        list.push(timetable);
    }

    let combined = combine_list(list);

    files::serialize_sync(&config.output_filename, &combined).context("saving output")?;

    Ok(())
}

/// merges all timetables into one.
/// We try to preserve ids of the first ones
pub fn combine_list(list: impl IntoIterator<Item = Timetable>) -> Timetable {
    let mut list = list.into_iter();

    let mut current = list.next().expect("missing at least one timetable");
    for new in list {
        current = combine_timetables(current, new);
    }
    current
}

/// Merges the new timetable into the current timetable
fn combine_timetables(current: Timetable, new: Timetable) -> Timetable {
    // Steps
    // 1: get mapping from new -> current
    // 2: set all current to disabled
    // 3: extend current and fill with new

    // Step 1: get mapping from new -> current
    // possibly creating new ids
    let (container_mapping, session_mapping) = determine_mapping(&current, &new);

    // creates
    // - a function which takes new_ids and produces the current_id (or equivalent)
    // - the total of number of ids
    let mk_complete_mapping = |current_new_mapping, num_current_items, num_new_items| {
        // mapping from (new-1) -> current
        let mut new_current_mapping = vec![0; num_new_items];
        for (current, new) in current_new_mapping {
            new_current_mapping[new as usize - 1] = current;
        }
        // add missing ids
        let mut total_ids = num_current_items;
        for current in &mut new_current_mapping {
            // we can check for zero since zero is not a valid id
            if *current == 0 {
                *current = total_ids as Id + 1;
                total_ids += 1;
            }
        }

        // create function
        let f = move |new_id: Id| new_current_mapping[new_id as usize - 1];
        (f, total_ids)
    };
    // map from new -> current
    let (container_mapping, total_containers) = mk_complete_mapping(
        container_mapping,
        current.containers.len(),
        new.containers.len(),
    );
    let (session_mapping, total_sessions) =
        mk_complete_mapping(session_mapping, current.sessions.len(), new.sessions.len());

    // Step 2: set all current to disabled
    let mut current = current;
    for session in &mut current.sessions {
        session.enabled = false;
    }
    for container in &mut current.containers {
        container.enabled = false;
    }

    // Step 3: extend and fill
    current
        .sessions
        .resize_with(total_sessions, Default::default);
    current
        .containers
        .resize_with(total_containers, Default::default);

    // fill in
    for mut session in new.sessions {
        let id = session_mapping(session.id);
        session.id = id;
        session.containers = session
            .containers
            .into_iter()
            .map(&container_mapping)
            .collect();
        current.sessions[id as usize - 1] = session;
    }
    for mut container in new.containers {
        let id = container_mapping(container.id);
        container.id = id;
        container.sessions = container
            .sessions
            .into_iter()
            .map(&session_mapping)
            .collect();
        current.containers[id as usize - 1] = container;
    }

    // return current
    current
}

/// Returns lists of (current, new) container ids and session ids
fn determine_mapping(current: &Timetable, new: &Timetable) -> (Vec<(Id, Id)>, Vec<(Id, Id)>) {
    let container_mapping = determine_container_mapping(&current.containers, &new.containers);
    let session_mapping =
        determine_session_mapping(&current.sessions, &new.sessions, &container_mapping);

    (container_mapping, session_mapping)
}

/// returns list of matching (current, new) container ids
fn determine_container_mapping(current: &[Container], new: &[Container]) -> Vec<(Id, Id)> {
    // we assume that two containers are the same if they have the same type and code.
    // This is the same as unify.rs
    let get_container_key =
        |container: &Container| (container.r#type.clone(), container.code.clone());

    // current_key -> current_id
    let current_key_ids: HashMap<_, _> = current
        .iter()
        .map(|c| (get_container_key(c), c.id))
        .collect();

    // for each of the new containers,
    // try to find a matching key
    new.iter()
        .filter_map(|nc| {
            let current_id = *current_key_ids.get(&get_container_key(nc))?;
            let new_id = nc.id;
            Some((current_id, new_id))
        })
        .sorted()
        .collect_vec()
}

/// Takes the container mapping and returns session mapping.
/// Recall mapping is a list of matching (current, new)
fn determine_session_mapping(
    current: &[Session],
    new: &[Session],
    container_mapping: &[(Id, Id)],
) -> Vec<(Id, Id)> {
    // Steps
    // 1. group by a key
    // 2. within each group, try to find best pairs
    let present_new_containers: HashSet<Id> = container_mapping.iter().map(|(_, id)| *id).collect();
    let container_mapping: HashMap<Id, Id> = container_mapping.iter().cloned().collect();

    // Step 1: group by key
    // key = (type, day, time, containers, flattened_tags)
    // the keys are have the new container_ids
    let _get_common_session_key = |session: &Session| {
        (
            session.r#type.clone(),
            session.day,
            session.timeperiod.clone(),
            session
                .tags
                .iter()
                .flat_map(|(k, v)| v.iter().map(move |t| (k.clone(), t.clone())))
                .sorted()
                .collect_vec(),
        )
    };
    let get_current_session_key = |session: &Session| {
        if session
            .containers
            .iter()
            .all(|c_id| container_mapping.contains_key(c_id))
        {
            let containers = session
                .containers
                .iter()
                .map(|c_id| container_mapping[c_id])
                .collect::<BTreeSet<_>>();
            Some((_get_common_session_key(session), containers))
        } else {
            None
        }
    };
    let get_new_session_key = |session: &Session| {
        if session
            .containers
            .iter()
            .all(|c_id| present_new_containers.contains(c_id))
        {
            Some((_get_common_session_key(session), session.containers.clone()))
        } else {
            None
        }
    };

    // group sessions by keys
    let current_grouped = current
        .iter()
        .filter_map(|s| Some((get_current_session_key(s)?, s)))
        .into_group_map();
    let new_grouped = new
        .iter()
        .filter_map(|s| Some((get_new_session_key(s)?, s)))
        .into_group_map();

    // Step 2: Try to find best pairs
    let mut session_mapping = Vec::new();
    for (key, current_sessions) in current_grouped {
        let Some(new_sessions) = new_grouped.get(&key) else {
            continue;
        };

        // For each pair of current, new, compute a distance
        let pairs_distances = itertools::iproduct!(&current_sessions, new_sessions)
            .filter_map(|(current, new)| {
                // sessions are exactly the same, except the weeks
                // We want to count the number that are not in both
                // This is (num in current) + (num in new) - 2*num_in_both
                // We can do this in a simple loop since the number of weeks should be small
                let num_in_both = current
                    .weeks
                    .iter()
                    .filter(|w| new.weeks.iter().contains(w))
                    .count();

                let distance = current.weeks.len() + new.weeks.len() - 2 * num_in_both;
                Some((distance, current.id, new.id))
            })
            .sorted_by_key(|(dist, _, _)| *dist);
        // note that the above is sorted from smallest distance to greatest

        // we now greedily take pairs
        let mut used_current = HashSet::new();
        let mut used_new = HashSet::new();
        for (_, current, new) in pairs_distances {
            if !used_current.contains(&current) && !used_new.contains(&new) {
                session_mapping.push((current, new));
                used_current.insert(current);
                used_new.insert(new);
            }
        }
    }

    // finally sort the mapping and return
    session_mapping.sort();
    session_mapping
}

#[cfg(test)]
mod tests {
    use super::*;

    /// This test ensures that if we have the same timetable twice, we get the identity mapping
    #[test]
    fn stable_test() {
        let timetable: Timetable =
            toml::from_str(include_str!("../test_data/2024-10-24-timetable.toml")).unwrap();

        let (container_mapping, session_mapping) = determine_mapping(&timetable, &timetable);

        let mk_id_mapping = |len| (1..=(len as Id)).map(|i| (i, i)).collect_vec();

        assert_eq!(container_mapping, mk_id_mapping(timetable.containers.len()));
        assert_eq!(session_mapping, mk_id_mapping(timetable.sessions.len()));

        assert_eq!(
            combine_timetables(timetable.clone(), timetable.clone()),
            timetable
        );
    }

    /// This test ensures that if we remap the ids in some manner, we get the same expected mapping
    #[test]
    fn mapped_ids() {
        let timetable: Timetable =
            toml::from_str(include_str!("../test_data/2024-10-24-timetable.toml")).unwrap();

        // chosen arbitrarily.
        // Note that we ensured that the timetable does not have 67 sessions/containers in it
        let prime = 67;
        let offset = 10;

        // lists of (original, new)
        let container_id_mapping = (1..=timetable.containers.len())
            .map(|i| {
                let map = (i * prime + offset) % timetable.containers.len() + 1;
                (i as Id, map as Id)
            })
            .collect_vec();
        let session_id_mapping = (1..=timetable.sessions.len())
            .map(|i| {
                let map = (i * prime + offset) % timetable.sessions.len() + 1;
                (i as Id, map as Id)
            })
            .collect_vec();

        // now we need to map the timetable
        // For each new id, find the original id and adjust the session/container
        let mapped_timetable = Timetable {
            sessions: (1..=timetable.sessions.len())
                .map(|new_id| {
                    let new_id = new_id as Id;
                    let original_id = session_id_mapping
                        .iter()
                        .find(|(_, i)| *i == new_id)
                        .unwrap()
                        .0;
                    let mut session = timetable.sessions[original_id as usize - 1].clone();
                    assert_eq!(session.id, original_id);

                    // mapping original to new
                    session.id = new_id;
                    session.containers = session
                        .containers
                        .into_iter()
                        .map(|cid| container_id_mapping[cid as usize - 1].1)
                        .collect();

                    session
                })
                .collect_vec(),
            containers: (1..=timetable.containers.len())
                .map(|new_id| {
                    let new_id = new_id as Id;
                    let original_id = container_id_mapping
                        .iter()
                        .find(|(_, i)| *i == new_id)
                        .unwrap()
                        .0;
                    let mut container = timetable.containers[original_id as usize - 1].clone();

                    // mapping original to new
                    container.id = new_id;
                    container.sessions = container
                        .sessions
                        .into_iter()
                        .map(|sid| session_id_mapping[sid as usize - 1].1)
                        .collect();

                    container
                })
                .collect_vec(),
        };

        let (container_mapping, session_mapping) = determine_mapping(&timetable, &mapped_timetable);

        assert_eq!(container_mapping, container_id_mapping);
        assert_eq!(session_mapping, session_id_mapping);

        assert_eq!(
            combine_timetables(timetable.clone(), mapped_timetable),
            timetable
        );
    }

    #[test]
    fn empty_current() {
        let timetable: Timetable =
            toml::from_str(include_str!("../test_data/2024-10-24-timetable.toml")).unwrap();

        let empty_timetable = Timetable::default();

        assert_eq!(
            combine_timetables(empty_timetable, timetable.clone()),
            timetable
        );
    }
    #[test]
    fn empty_new() {
        let timetable: Timetable =
            toml::from_str(include_str!("../test_data/2024-10-24-timetable.toml")).unwrap();

        let empty_timetable = Timetable::default();

        let mut disabled_timetable = timetable.clone();
        for session in &mut disabled_timetable.sessions {
            session.enabled = false;
        }
        for container in &mut disabled_timetable.containers {
            container.enabled = false;
        }

        assert_eq!(
            combine_timetables(timetable, empty_timetable),
            disabled_timetable
        );
    }
}

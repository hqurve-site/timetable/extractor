//! This file takes the parsed pages and turns it into a complete timetable.
//! Note that this deduplicates the data and assigns ids.
//! The output should be stable.
//! Tracking information to rebuild the original pages are also outputted.

use std::{
    collections::{BTreeMap, BTreeSet, HashMap, HashSet},
    hash::Hash,
};

use anyhow::{Context, Result};
use itertools::Itertools;
use serde::{Deserialize, Serialize};

use crate::{
    parse::PageData,
    timetable::{Container, Day, Id, Session, Timeperiod, Timestamp, Timetable, Week},
};

#[derive(Debug, Deserialize)]
pub struct Config {
    /// Sessions are first grouped by key=(type, day, time, containers).
    /// There is still (weeks, tags) not handled as yet, so there may multiple
    /// which match the same key.
    ///
    /// We assume that tags should not be combined
    /// The following flag indicates whether to merge weeks
    pub session_merge_weeks: bool,

    /// The timestamp on the parsed pages are naive and do not have timezones.
    /// Note: local time = utc + offset
    pub page_timestamp_offset_east: i32,
}

#[derive(Debug, Serialize, Hash, Clone, PartialEq, Eq, PartialOrd, Ord)]
#[allow(clippy::enum_variant_names)]
pub enum Warning {
    MultipleContainerNames {
        id: ContainerIdentifer,
        names: Vec<String>,
    },
    MultipleContainerTimestamps {
        id: ContainerIdentifer,
        timestamps: Vec<Timestamp>,
    },
    MultipleSessionWeeks {
        id: SessionIdentifer,
        tags: BTreeMap<String, Vec<String>>,
        // for each duplicate: weeks -> locations
        weeks: Vec<(Vec<Week>, Vec<PageId>)>,
    },
}

#[derive(Debug, Serialize, Deserialize, Clone, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct SessionIdentifer {
    pub r#type: String,
    pub day: Day,
    pub timeperiod: Timeperiod,
    pub containers: BTreeSet<ContainerIdentifer>,
}
#[derive(Debug, Serialize, Deserialize, Clone, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct ContainerIdentifer {
    pub r#type: String,
    pub code: String,
}

#[derive(Clone, Serialize, Deserialize, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct ContainerExtra {
    pub name: Option<String>,
    pub timestamp: Option<Timestamp>,
}

#[derive(Clone, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord)]
pub struct SimpleContainer {
    pub id: ContainerIdentifer,
    pub extra: ContainerExtra,
}

#[derive(Clone, Serialize, Deserialize, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct SessionExtra {
    pub weeks: Vec<Week>,
    pub tags: BTreeMap<String, Vec<String>>,
}

#[derive(Clone, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord)]
pub struct SimpleSession {
    pub id: SessionIdentifer,

    pub extra: SessionExtra,
}

#[derive(Serialize, Deserialize)]
pub struct ResourcePageContents {
    pub resource_id: String,
    pub page: usize,
    pub main_container: Id,
    pub sessions: Vec<Id>,
}

type PageId = (String, usize);
#[derive(Serialize, Deserialize, Default)]
pub struct ResourceTrackingInfo {
    pages: Vec<ResourcePageContents>,
}

pub fn run_unify(config: &Config) -> Result<()> {
    // load list of successful parses and listing
    let successful_parses: Vec<String> =
        crate::files::deserialize_sync(crate::files::SUCCESSFUL_PARSE_LIST_FILE)
            .context("getting successful_parses")?;

    let mut parsed_pdfs: Vec<(String, Vec<PageData>)> = Vec::new();
    for resource_id in successful_parses {
        let parse_path = crate::files::timetable_parse_pattern(&resource_id);
        let pages: Vec<PageData> = crate::files::deserialize_sync(&parse_path)
            .with_context(|| format!("loading parsed pages for {resource_id:?}"))?;
        parsed_pdfs.push((resource_id, pages));
    }

    // collect into one
    let (timetable, tracking_info, warnings) = collect_timetable(config, parsed_pdfs);

    // save
    crate::files::serialize_sync(crate::files::INITIAL_TIMETABLE_FILE, &timetable)
        .context("saving timetable")?;
    crate::files::serialize_sync(
        crate::files::INITIAL_TIMETABLE_TRACKING_INFO_FILE,
        &tracking_info,
    )
    .context("saving tracking_info")?;
    crate::files::serialize_sync(crate::files::UNIFY_WARNINGS_FILE, &warnings)
        .context("saving warnings")?;

    Ok(())
}

pub fn collect_timetable(
    config: &Config,
    parsed_pdfs: Vec<(String, Vec<PageData>)>,
) -> (Timetable, ResourceTrackingInfo, Vec<Warning>) {
    // This function
    // 1. Extracts sessions and containers from the parsed data
    //    and groups them by some identifier
    // 2. Merges the grouped containers and assign ids
    // 3. Merge/expand grouped sessions and assign ids
    //    and add relationships with containers
    let mut warnings = Vec::new();

    // Stage 1: extract sessions and data from parsed data and group by identifiers
    // For each identifier, store the set of extra info -> page_id
    let mut sessions: BTreeMap<SessionIdentifer, HashMap<SessionExtra, HashSet<PageId>>> =
        BTreeMap::new();
    // for each identifier store the set of unique extra info
    let mut containers: BTreeMap<ContainerIdentifer, HashSet<ContainerExtra>> = BTreeMap::new();
    // for each page, store what the container identifiers are
    let mut pages_container_identifers: Vec<(PageId, ContainerIdentifer)> = Vec::new();

    for (resource_id, pages) in parsed_pdfs {
        for (page_i, page_data) in pages.into_iter().enumerate() {
            let page_id = (resource_id.clone(), page_i);

            // get the (page container identifer, sessions, containers)
            let (meta_id, s, c) = collect_from_page(config, page_data);
            pages_container_identifers.push((page_id.clone(), meta_id));

            for session in s {
                sessions
                    .entry(session.id)
                    .or_default()
                    .entry(session.extra)
                    .or_default()
                    .insert(page_id.clone());
            }
            for container in c {
                containers
                    .entry(container.id)
                    .or_default()
                    .insert(container.extra);
            }
        }
    }

    // Stage 2: Merges the containers and assigns ids
    let mut container_id_map: HashMap<ContainerIdentifer, Id> = HashMap::new();
    let mut container_map: BTreeMap<Id, Container> = BTreeMap::new();

    for (id, (container_id, extras)) in containers.into_iter().enumerate() {
        let id = (id + 1) as Id;

        let (extra, w) = unify_containers(container_id.clone(), extras);

        warnings.extend(w);

        container_id_map.insert(container_id.clone(), id);

        container_map.insert(
            id,
            Container {
                id,
                last_modified_timestamp: extra.timestamp,
                enabled: true,
                r#type: container_id.r#type,
                code: container_id.code,
                name: extra.name,
                sessions: BTreeSet::new(),
            },
        );
    }

    // setup pages tracking info
    let mut tracking_info: BTreeMap<PageId, ResourcePageContents> = BTreeMap::new();
    for (location, identifer) in pages_container_identifers {
        tracking_info.insert(
            location.clone(),
            ResourcePageContents {
                resource_id: location.0,
                page: location.1,
                main_container: container_id_map[&identifer],
                sessions: Vec::new(),
            },
        );
    }

    // Stage 3: Merge/expand grouped sessions and add links to containers
    let mut counter = 1..;
    let mut session_map: BTreeMap<Id, Session> = BTreeMap::new();
    for (session_id, sessions) in sessions {
        // unify_sessions groups sessions as necessary.
        let (mut sessions, w) = unify_sessions(config, session_id.clone(), sessions);
        warnings.extend(w);

        // iterate through the sessions and assign ids one time
        sessions.sort();
        for ((extra, locations), id) in sessions.into_iter().zip(counter.by_ref()) {
            let id = id as Id;

            let containers = session_id.containers.iter().map(|container| *container_id_map.get(container).expect("When extracting the containers from each page, we created one whenever a session referenced it")).collect();
            let session = Session {
                id,
                last_modified_timestamp: 0,
                enabled: true,
                r#type: session_id.r#type.clone(),
                day: session_id.day,
                timeperiod: session_id.timeperiod.clone(),
                containers,
                weeks: extra.weeks,
                tags: extra.tags,
            };
            for c_id in &session.containers {
                container_map.get_mut(c_id).unwrap().sessions.insert(id);
            }
            session_map.insert(id, session);

            // store in relevant pages
            for location in locations {
                tracking_info
                    .get_mut(&location)
                    .expect("all pages exist")
                    .sessions
                    .push(id);
            }
        }
    }

    // Step 4: return
    let timetable = Timetable {
        sessions: session_map.into_values().collect_vec(),
        containers: container_map.into_values().collect_vec(),
    };
    let tracking_info = ResourceTrackingInfo {
        pages: tracking_info.into_values().collect_vec(),
    };

    warnings.sort();

    (timetable, tracking_info, warnings)
}

// takes the page data
// returns
// - indentifier of page container
// - list of sessions in the page
// - containers (including those referenced by sessions)
fn collect_from_page(
    config: &Config,
    page_data: PageData,
) -> (ContainerIdentifer, Vec<SimpleSession>, Vec<SimpleContainer>) {
    let mut sessions = Vec::new();
    let mut containers = Vec::new();

    // collect from meta
    let meta_id = ContainerIdentifer {
        r#type: page_data.meta.page_type,
        code: page_data.meta.code,
    };
    containers.push(SimpleContainer {
        id: meta_id.clone(),
        extra: ContainerExtra {
            name: page_data.meta.name,
            timestamp: {
                let timestamp = page_data.meta.publish_time;
                let offset = chrono::FixedOffset::east_opt(config.page_timestamp_offset_east)
                    .expect("invalid timestamp offset");
                let timestamp = timestamp.and_local_timezone(offset).unwrap();
                Some(timestamp.timestamp())
            },
        },
    });

    // collect from session
    for session in page_data.sessions {
        // cleanup
        let weeks = session.weeks.into_iter().sorted().collect();
        let s_containers: BTreeSet<ContainerIdentifer> = session
            .containers
            .into_iter()
            .map(|(r#type, code)| ContainerIdentifer { code, r#type })
            .collect();
        let tags = session
            .tags
            .into_iter()
            .into_group_map()
            .into_iter()
            .collect();

        // ensure we have referenced containers
        for container_id in &s_containers {
            containers.push(SimpleContainer {
                id: container_id.clone(),
                extra: ContainerExtra {
                    name: None,
                    timestamp: None,
                },
            });
        }

        sessions.push(SimpleSession {
            id: SessionIdentifer {
                r#type: session.r#type,
                day: session.day,
                timeperiod: Timeperiod {
                    start: session.start,
                    end: session.end,
                },
                containers: s_containers,
            },
            extra: SessionExtra { weeks, tags },
        });
    }

    (meta_id, sessions, containers)
}

// Input is a set containing at least one simple session container where all share the same id.
// We want to get a single container and produce warnings if necessary
fn unify_containers(
    id: ContainerIdentifer,
    containers: HashSet<ContainerExtra>,
) -> (ContainerExtra, Vec<Warning>) {
    let mut warnings = Vec::new();

    let mut names = HashSet::new();
    let mut timestamps = HashSet::new();

    // order does not matter as we are dumping into two sets anyhow
    for sc in containers {
        if let Some(name) = sc.name {
            names.insert(name);
        }
        if let Some(timestamp) = sc.timestamp {
            timestamps.insert(timestamp);
        }
    }

    // sort for determinism
    let names = names.into_iter().sorted().collect_vec();
    let timestamps = timestamps.into_iter().sorted().collect_vec();

    let name = names.first().cloned();
    let timestamp = timestamps.last().cloned();

    if names.len() > 1 {
        tracing::warn!(
            "Selecting first of multiple names for container {:?}: {:?}",
            id,
            names
        );
        warnings.push(Warning::MultipleContainerNames {
            id: id.clone(),
            names,
        });
    };
    if timestamps.len() > 1 {
        tracing::warn!(
            "Selecting newest of multiple timestamps for container {:?}: {:?}",
            id,
            timestamps
        );
        warnings.push(Warning::MultipleContainerTimestamps { id, timestamps });
    }

    let container = ContainerExtra { name, timestamp };

    (container, warnings)
}

// Input is a set from session_extra -> [locations], which contains at least one entry.
// We want to get merge sessions and produce warnings if necessary
fn unify_sessions(
    config: &Config,
    id: SessionIdentifer,
    sessions: HashMap<SessionExtra, HashSet<(String, usize)>>,
) -> (Vec<(SessionExtra, Vec<(String, usize)>)>, Vec<Warning>) {
    let mut warnings = Vec::new();

    // there are two keys for each session: weeks and tags
    // Tags are always unique, but sometimes, we may choose to merge the weeks
    // of multiple session

    // the entries are not sorted as yet, but each item is deterministic
    // (ie inner vectors are sorted)
    let mut out_sessions = Vec::new();

    if config.session_merge_weeks {
        // in this case we must merge weeks together

        // First, group by tags then weeks
        // tags -> weeks -> locations
        let mut by_tags: HashMap<_, HashMap<_, HashSet<_>>> = HashMap::new();
        for (e, locations) in sessions {
            by_tags
                .entry(e.tags)
                .or_default()
                .entry(e.weeks)
                .or_default()
                .extend(locations);
        }

        // Next, produce a session for each tag and produce appropriate warnings
        for (tags, week_locations_pairs) in by_tags {
            if week_locations_pairs.len() > 1 {
                tracing::warn!("Combining multiple sets of weeks for {id:?}, {tags:?}: {week_locations_pairs:?}");
                warnings.push(Warning::MultipleSessionWeeks {
                    id: id.clone(),
                    tags: tags.clone(),
                    weeks: week_locations_pairs
                        .iter()
                        .map(|(k, v)| (k.clone(), v.iter().cloned().sorted().collect_vec()))
                        .sorted()
                        .collect_vec(),
                });
            }
            let mut weeks = BTreeSet::new();
            let mut locations = BTreeSet::new();
            for (ws, ls) in week_locations_pairs {
                weeks.extend(ws);
                locations.extend(ls);
            }

            out_sessions.push((
                SessionExtra {
                    weeks: weeks.into_iter().collect_vec(),
                    tags,
                },
                locations.into_iter().collect_vec(),
            ));
        }
    } else {
        // in this case, there is nothing to be done
        for (e, locations) in sessions {
            let locations = locations.into_iter().sorted().collect_vec();
            out_sessions.push((e, locations));
        }
    }

    // sort sessions
    out_sessions.sort();

    (out_sessions, warnings)
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn unify_test() {
        #[derive(Deserialize)]
        struct ParsedDocument {
            pages: Vec<PageData>,
        }
        let mk_resources = || {
            [
                ("m3038", include_str!("../test_data/2024-10-24-m3038.toml")),
                ("r3496", include_str!("../test_data/2024-10-24-r3496.toml")),
                ("r3513", include_str!("../test_data/2024-10-24-r3513.toml")),
            ]
            .map(|(rid, s)| {
                let rid = rid.to_string();
                let parsed: ParsedDocument = toml::from_str(s).unwrap();
                (rid, parsed.pages)
            })
            .into_iter()
            .collect_vec()
        };

        let (observed_timetable, observed_tracking_info, warnings) = collect_timetable(
            &Config {
                session_merge_weeks: false,
                page_timestamp_offset_east: -4 * 60 * 60,
            },
            mk_resources(),
        );

        let session_map: BTreeMap<_, _> = observed_timetable
            .sessions
            .iter()
            .map(|s| (s.id, s))
            .collect();
        let container_map: BTreeMap<_, _> = observed_timetable
            .containers
            .iter()
            .map(|c| (c.id, c))
            .collect();

        // We do some property based tests
        // We can first check if all contents of each page is there
        let resources: BTreeMap<PageId, PageData> = mk_resources()
            .into_iter()
            .flat_map(|(rid, pages)| {
                pages
                    .into_iter()
                    .enumerate()
                    .map(move |(i, pagedata)| ((rid.clone(), i), pagedata))
            })
            .collect();

        assert_eq!(
            resources.keys().cloned().collect_vec(),
            observed_tracking_info
                .pages
                .iter()
                .map(|page| (page.resource_id.clone(), page.page))
                .collect_vec()
        );

        for ResourcePageContents {
            resource_id,
            page,
            main_container,
            sessions,
        } in observed_tracking_info.pages
        {
            let page_id = (resource_id, page);
            let page_data = &resources[&page_id];
            // ensure main container is the same
            assert_eq!(page_data.meta.code, container_map[&main_container].code);

            // ensure each of the sessions exist
            assert_eq!(page_data.sessions.len(), sessions.len());
            for s_id in &sessions {
                let session = &session_map[s_id];
                let container_codes = session
                    .containers
                    .iter()
                    .map(|c_id| &container_map[c_id].code)
                    .sorted()
                    .collect_vec();

                // see if we can find in page_data
                let found = page_data.sessions.iter().any(|simple_session| {
                    simple_session.day == session.day
                        && simple_session.start == session.timeperiod.start
                        && simple_session.end == session.timeperiod.end
                        && simple_session.r#type == session.r#type
                        && simple_session.weeks == session.weeks
                        && simple_session
                            .containers
                            .iter()
                            .map(|(_, code)| code)
                            .sorted()
                            .collect_vec()
                            == container_codes
                        && simple_session.tags.iter().cloned().sorted().collect_vec()
                            == session
                                .tags
                                .iter()
                                .flat_map(|(k, v)| v.iter().map(|t| (k.clone(), t.clone())))
                                .sorted()
                                .collect_vec()
                });

                if !found {
                    panic!("page:{page_id:?}: No matching simple_session for {session:?}. containers={container_codes:?}")
                }
            }
        }
        // end matching page contents

        // ensure we have the correct number of sessions and containers
        // Note that we do not double count sessions in STAT6110
        assert_eq!(session_map.len(), (38 + 37));

        // finally, ensure stable output
        // println!("{}", toml::to_string_pretty(&observed_timetable).unwrap());
        // panic!();
        let expected_timetable: Timetable =
            toml::from_str(include_str!("../test_data/2024-10-24-timetable.toml")).unwrap();
        assert_eq!(expected_timetable, observed_timetable);

        assert!(warnings.is_empty());
    }
}

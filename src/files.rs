use std::path::Path;

use anyhow::{Context, Result};
use serde::{de::DeserializeOwned, Serialize};

pub const PARSE_CONFIG: &str = "parse_config";
pub const UNIFY_CONFIG: &str = "unify_config";
pub const MERGE_CONFIG: &str = "merge_config";

// pub const LISTING_XML: &str = "download/listing.xml";
pub const LISTING_FILE: &str = "download/listing";
pub const SUCCESSFUL_DOWNLOADS_LIST_FILE: &str = "download/successful_downloads";
// pub const FAILED_DOWNLOADS_LIST_FILE: &str = "download/failed_downloads";

pub const SUCCESSFUL_PARSE_LIST_FILE: &str = "parse/successful_parses";
pub const FAILED_PARSE_LIST_FILE: &str = "parse/failed_parses";

pub const INITIAL_TIMETABLE_FILE: &str = "timetable/initial";
pub const UNIFY_WARNINGS_FILE: &str = "timetable/unify_warnings";
pub const INITIAL_TIMETABLE_TRACKING_INFO_FILE: &str = "timetable/initial_tracking_info";

pub fn timetable_pdf_pattern(resource_id: &str) -> String {
    format!("download/resources/{resource_id}.pdf")
}
pub fn timetable_parse_pattern(resource_id: &str) -> String {
    format!("parse/resources/{resource_id}")
}
pub fn timetable_parse_debug_pattern(resource_id: &str, page: usize, stage: &str) -> String {
    format!("parse/resources_debug/{resource_id}_{page}_{stage}")
}

// pub fn timetable_debug_pattern(resource_id: &str, stage: &str, page: usize) -> String {
//     format!("parse/debug/{resource_id}__debug_{stage}__{page}")
// }

// pub async fn serialize<T: Serialize>(path: &str, data: &T) -> Result<()> {
//     let path = format!("{path}.json");
//     tokio::fs::create_dir_all(Path::new(&path).parent().context("unable to get parent")?)
//         .await
//         .context("creating parents")?;
//     tokio::fs::write(&path, serde_json::to_string(&data).context("seralizing")?)
//         .await
//         .with_context(|| format!("writing to {path:?}"))
// }
// pub async fn deserialize<T: DeserializeOwned>(path: &str) -> Result<T> {
//     let path = format!("{path}.json");
//     serde_json::from_str(
//         tokio::fs::read_to_string(&path)
//             .await
//             .with_context(|| format!("while reading from {path:?}"))?
//             .as_str(),
//     )
//     .context("while parsing")
// }

pub fn serialize_sync<T: Serialize>(path: &str, data: &T) -> Result<()> {
    let path = format!("{path}.json");
    std::fs::create_dir_all(Path::new(&path).parent().context("unable to get parent")?)
        .context("creating parents")?;
    std::fs::write(&path, serde_json::to_string(&data).context("seralizing")?)
        .with_context(|| format!("writing to {path:?}"))
}
pub fn deserialize_sync<T: DeserializeOwned>(path: &str) -> Result<T> {
    let path = format!("{path}.json");
    serde_json::from_str(
        std::fs::read_to_string(&path)
            .with_context(|| format!("while reading from {path:?}"))?
            .as_str(),
    )
    .context("while parsing")
}

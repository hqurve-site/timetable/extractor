//! This module downloads and parses the listing and then downloads all associated pdfs

use serde::{Deserialize, Serialize};

/// This structure corresponds to an entry in the downloaded listing.
/// Sadly, many things may be missing
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Resource {
    pub id: String,
    pub name: String,
    // (type_id, type)
    pub r#type: Option<(String, String)>,
    // (department_id, department)
    pub department: Option<(usize, String)>,
    // (faculty_id, faculty)
    pub faculty: Option<(usize, String)>,
    pub link: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Listing {
    pub timestamp: i64,
    pub resources: Vec<Resource>,
}

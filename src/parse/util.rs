use std::{cmp::Ordering, ops::Range};

use serde::{Deserialize, Serialize};

use super::Number;

pub type Vek2 = vek::Vec2<Number>;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Text {
    pub pos: Vek2,
    pub string: String,
}
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Path {
    /// a sequence of start, end pairs
    pub segments: Vec<(Vek2, Vek2)>,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub struct Rectangle {
    x: Number, // left
    y: Number, // bottom
    width: Number,
    height: Number,
}

/*
 *
 *    Currently, up is positive y.
 *    Please limit the orientation to this file.
 *
 *    ^
 *    |
 *    |
 *    |
 *    o ------------ >
 */

pub fn order_top_bottom(a: &Number, b: &Number) -> Ordering {
    a.partial_cmp(b).unwrap().reverse()
}
pub fn order_left_right(a: &Number, b: &Number) -> Ordering {
    a.partial_cmp(b).unwrap()
}
pub fn is_above(a: Number, b: Number) -> bool {
    a > b
}
#[allow(dead_code)]
pub fn is_left(a: Number, b: Number) -> bool {
    a < b
}
#[allow(dead_code)]
pub fn is_right(a: Number, b: Number) -> bool {
    a > b
}
pub fn is_below(a: Number, b: Number) -> bool {
    a < b
}

impl Rectangle {
    pub fn from_points(p1: Vek2, p2: Vek2) -> Self {
        let mid_x = (p1.x + p2.x) / 2.0;
        let mid_y = (p1.y + p2.y) / 2.0;

        let width = (p1.x - p2.x).abs();
        let height = (p1.y - p2.y).abs();

        Self {
            x: mid_x - width / 2.0,
            y: mid_y - height / 2.0,
            width,
            height,
        }
    }
    pub fn from_boundaries(x: Range<Number>, y: Range<Number>) -> Self {
        Self::from_points(
            Vek2 {
                x: x.start,
                y: y.start,
            },
            Vek2 { x: x.end, y: y.end },
        )
    }
    pub fn top(&self) -> Number {
        self.y + self.height
    }
    pub fn bottom(&self) -> Number {
        self.y
    }
    pub fn left(&self) -> Number {
        self.x
    }
    pub fn right(&self) -> Number {
        self.x + self.width
    }
    pub fn width(&self) -> Number {
        self.width
    }
    pub fn height(&self) -> Number {
        self.height
    }
    fn corners(&self) -> [Vek2; 4] {
        let Self {
            x,
            y,
            width,
            height,
        } = *self;
        [
            Vek2::new(x, y),
            Vek2::new(x + width, y),
            Vek2::new(x + width, y + height),
            Vek2::new(x, y + height),
        ]
    }
    pub fn contains(&self, point: &Vek2, epsilon: Number) -> bool {
        let e = epsilon;
        (self.left() - e < point.x)
            && (point.x < self.right() + e)
            && (self.bottom() - e < point.y)
            && (point.y < self.top() + e)
    }
    pub fn contains_rect(&self, other: &Self, epsilon: Number) -> bool {
        let [q1, _, q2, _] = other.corners();

        self.contains(&q1, epsilon) && self.contains(&q2, epsilon)
    }
}

use anyhow::{Context, Result};
use itertools::Itertools;
use serde::{Deserialize, Deserializer};

const REGEX_SIZE_LIMIT: usize = 10 * (1 << 24);
const REGEX_DFA_SIZE_LIMIT: usize = 10 * (1 << 24);

/// This type is used to find a match.
/// After finding a match, it determines it returns the appropriate data,
/// possibly adjusting the value if necessary
#[derive(Debug)]
pub struct RegexMap<T> {
    // this regex just finds the items
    find_regex: regex::Regex,

    // this regex is used to figure out which was a match
    match_regex: regex::RegexSet,

    mappings: Vec<(regex::Regex, MappingMode, T)>,
}
#[derive(Debug, Deserialize)]
enum MappingMode {
    Literal,
    Template,
}
pub struct Match<T> {
    pub range: std::ops::Range<usize>,
    pub value: T,
}
impl<T> RegexMap<T> {
    fn new(raw_mappings: Vec<(String, MappingMode, T)>) -> Result<Self> {
        let find_regex =
            regex::RegexBuilder::new(raw_mappings.iter().map(|(r, _, _)| r).join("|").as_str())
                .size_limit(REGEX_SIZE_LIMIT)
                .dfa_size_limit(REGEX_DFA_SIZE_LIMIT)
                .build()
                .context("failed to build find_regex")?;
        let match_regex =
            regex::RegexSetBuilder::new(raw_mappings.iter().map(|(r, _, _)| format!("^{r}$")))
                .size_limit(REGEX_SIZE_LIMIT)
                .dfa_size_limit(REGEX_DFA_SIZE_LIMIT)
                .build()
                .context("failed to build match_regex")?;

        let mappings = raw_mappings
            .into_iter()
            .map(|(r, m, t)| {
                let regex = regex::Regex::new(format!("^{r}$").as_str())
                    .with_context(|| format!("building regex for {r:?}"))?;

                Ok((regex, m, t))
            })
            .collect::<Result<_>>()
            .context("failed to build individual regexes")?;

        Ok(Self {
            find_regex,
            match_regex,
            mappings,
        })
    }
    pub fn find(&self, text: &str) -> Option<Match<T>>
    where
        T: Expandable,
    {
        // first find
        let initial_match = self.find_regex.find(text)?;

        // Determine which matched
        let index_matched = self
            .match_regex
            .matches(initial_match.as_str())
            .iter()
            .next()
            .expect("at least one should match since we got an initial match");

        let (r, mode, t) = &self.mappings[index_matched];
        let value = match mode {
            MappingMode::Literal => t.clone(),
            MappingMode::Template => {
                let captures = r
                    .captures(initial_match.as_str())
                    .expect("the regex matched earlier");
                t.expand(&captures)
            }
        };

        Some(Match {
            range: initial_match.range(),
            value,
        })
    }
}

/// This type is used to determine which regex matches and return the cleaned version (if possible)
#[derive(Debug)]
pub struct RegexCleaner {
    // this regex is used to figure out which was a match
    match_regex: regex::RegexSet,

    // regex: Regex,
    mappings: Vec<(regex::Regex, MappingMode, String)>,
}
impl RegexCleaner {
    fn new(raw_mappings: Vec<(String, MappingMode, String)>) -> Result<Self> {
        let match_regex =
            regex::RegexSetBuilder::new(raw_mappings.iter().map(|(r, _, _)| format!("^{r}$")))
                .size_limit(REGEX_SIZE_LIMIT)
                .dfa_size_limit(REGEX_DFA_SIZE_LIMIT)
                .build()
                .context("failed to build match_regex")?;

        let mappings = raw_mappings
            .into_iter()
            .map(|(r, m, t)| {
                let regex = regex::Regex::new(format!("^{r}$").as_str())
                    .with_context(|| format!("building regex for {r:?}"))?;

                Ok((regex, m, t))
            })
            .collect::<Result<_>>()
            .context("failed to build individual regexes")?;

        Ok(Self {
            match_regex,
            mappings,
        })
    }
    pub fn clean(&self, text: &str) -> String {
        // Determine if any matched
        let index_matched = self.match_regex.matches(text).iter().next();

        if let Some(index_matched) = index_matched {
            let (r, mode, v) = &self.mappings[index_matched];

            match mode {
                MappingMode::Literal => v.to_string(),
                MappingMode::Template => {
                    let captures = r.captures(text).expect("the regex matched earlier");
                    v.expand(&captures)
                }
            }
        } else {
            // fallback to original if no match
            text.to_string()
        }
    }
}

pub trait Expandable: Clone {
    fn expand(&self, captures: &regex::Captures) -> Self;
}
impl Expandable for String {
    fn expand(&self, captures: &regex::Captures) -> Self {
        let mut dest = String::new();
        captures.expand(self, &mut dest);
        dest
    }
}
impl<A: Clone, B: Clone> Expandable for Vec<(A, B, String)> {
    fn expand(&self, captures: &regex::Captures) -> Self {
        self.iter()
            .map(|(a, b, s)| (a.clone(), b.clone(), s.expand(captures)))
            .collect()
    }
}

impl<'de, T: Deserialize<'de>> Deserialize<'de> for RegexMap<T> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let raw_mappings: Vec<(String, MappingMode, T)> = Vec::deserialize(deserializer)?;

        Self::new(raw_mappings).map_err(serde::de::Error::custom)
    }
}

impl<'de> Deserialize<'de> for RegexCleaner {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let raw_mappings: Vec<(String, MappingMode, String)> = Vec::deserialize(deserializer)?;

        Self::new(raw_mappings).map_err(serde::de::Error::custom)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn regex_mapper_test() {
        let raw_mappings = vec![
            (
                "[hH]\\s*[iI]".to_string(),
                MappingMode::Literal,
                "Hi".to_string(),
            ),
            (
                "hello\\s*world".to_string(),
                MappingMode::Literal,
                "hello world".to_string(),
            ),
            (
                "STAT\\s*(\\d{4})".to_string(),
                MappingMode::Template,
                "STAT $1".to_string(),
            ),
        ];

        let mapper = RegexMap::new(raw_mappings).unwrap();

        let samples = [
            (" hello world", "hello world", 1..12),
            ("h i hello world", "Hi", 0..3),
            ("H i hello world", "Hi", 0..3),
            ("hello     world h i", "hello world", 0..15),
            ("STAT6110 h i", "STAT 6110", 0..8),
        ];

        for (haystack, key, range) in samples {
            let m = mapper.find(haystack).unwrap();
            assert_eq!(m.range, range);
            assert_eq!(m.value, key);
        }
    }

    #[test]
    fn regex_cleaner_test() {
        let raw_mappings = vec![
            (
                "[hH]\\s*[iI]".to_string(),
                MappingMode::Literal,
                "Hi".to_string(),
            ),
            (
                "hello\\s*world".to_string(),
                MappingMode::Literal,
                "hello world".to_string(),
            ),
            (
                "STAT\\s*(\\d{4})".to_string(),
                MappingMode::Template,
                "STAT $1".to_string(),
            ),
        ];

        let mapper = RegexCleaner::new(raw_mappings).unwrap();

        let samples = [
            ("helloworld", "hello world"),
            ("random text", "random text"),
            (" hello world", " hello world"), // should not change
            ("h i", "Hi"),
            ("H i", "Hi"),
            ("hello     world", "hello world"),
            ("STAT6110", "STAT 6110"),
        ];

        for (haystack, key) in samples {
            let m = mapper.clean(haystack);
            assert_eq!(m, key);
        }
    }
}

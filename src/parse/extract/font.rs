// taken from https://github.com/pdf-rs/pdf_tools/blob/cb105ca25bcc89a315093dfcd527d155234aeb8c/src/lib.rs

use lopdf::{Document, ObjectId};
use std::collections::HashMap;

use anyhow::{anyhow, Context, Result};

#[derive(Default)]
enum Decoder<'a> {
    Encoding(lopdf::Encoding<'a>),
    #[default]
    None,
}

#[derive(Default)]
pub struct FontInfo<'a> {
    decoder: Decoder<'a>,
}

impl FontInfo<'_> {
    pub fn decode(&self, data: &[u8]) -> Result<String> {
        let mut s = String::new();
        self._decode(data, &mut s).map(|_| s)
    }
    pub fn _decode(&self, data: &[u8], out: &mut String) -> Result<()> {
        /// From https://docs.rs/pdf/0.9.0/src/pdf/font.rs.html#517-521
        /// helper function to decode UTF-16-BE data
        /// takes a slice of u8 and returns an iterator for char or an decoding error
        pub fn utf16be_to_char(
            data: &[u8],
        ) -> impl Iterator<Item = std::result::Result<char, std::char::DecodeUtf16Error>> + '_
        {
            char::decode_utf16(data.chunks(2).map(|w| u16::from_be_bytes([w[0], w[1]])))
        }

        match &self.decoder {
            Decoder::Encoding(encoding) => {
                let mut out2 = encoding
                    .bytes_to_string(data)
                    .context("decoding using lopdf::encoding")?;
                std::mem::swap(out, &mut out2);
                Ok(())
            }
            Decoder::None => {
                if data.starts_with(&[0xfe, 0xff]) {
                    utf16be_to_char(&data[2..]).try_for_each(|r| {
                        r.map_or(Err(anyhow!("Utf16Decode error")), |c| {
                            out.push(c);
                            Ok(())
                        })
                    })
                } else if let Ok(text) = std::str::from_utf8(data) {
                    out.push_str(text);
                    Ok(())
                } else {
                    Err(anyhow!("Utf16Decode error"))
                }
            }
        }
    }
}
pub struct FontCache<'a> {
    fonts: HashMap<Vec<u8>, FontInfo<'a>>,
    default_font: FontInfo<'a>, // simply fontInfo::default
}

impl<'a> FontCache<'a> {
    pub fn new(document: &'a Document, page_id: ObjectId) -> Result<Self> {
        let mut fonts = HashMap::new();

        // get page fonts
        // there are different font types (table 110)
        // The handling is based on the handing by the pdf_tools reference (see top of file).
        // I am not sure if this is sufficient
        for (name, font_dict) in document
            .get_page_fonts(page_id)
            .context("getting page fonts")?
        {
            let encoding = font_dict
                .get_font_encoding(document)
                .context("getting font encoding")?;

            // add to fonts map
            fonts.insert(
                name,
                FontInfo {
                    decoder: Decoder::Encoding(encoding),
                },
            );
        }

        // TODO: get fonts referenced by graphics_states

        Ok(Self {
            fonts,
            default_font: FontInfo::default(),
        })
    }

    pub fn get_default_font(&self) -> &FontInfo {
        &self.default_font
    }

    pub fn get_by_font_name(&self, name: &[u8]) -> &FontInfo {
        self.fonts.get(name).unwrap_or(&self.default_font)
    }
}

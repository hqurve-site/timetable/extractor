mod font;

use anyhow::{anyhow, bail, Context};
use itertools::Itertools;
use lopdf::{content::Content, Document, Object, ObjectId};
use serde::{Deserialize, Serialize};
use vek::{Mat3 as Matrix, Vec3};

use self::font::{FontCache, FontInfo};

use anyhow::Result;

use super::{
    util::{Path, Text, Vek2},
    ExtractConfig, Number,
};

/*
 * Extracts lines and text from pdf
 * PDF Reference https://opensource.adobe.com/dc-acrobat-sdk-docs/pdfstandards/PDF32000_2008.pdf
 */

type Vek = Vec3<Number>;

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct ExtractedInfo {
    pub paths: Vec<Path>,
    pub text: Vec<Text>,
}

impl ExtractedInfo {
    #[tracing::instrument(skip_all)]
    pub fn extract_from(
        document: &Document,
        page_id: ObjectId,
        config: &ExtractConfig,
    ) -> Result<Self> {
        let mut info = Self::default();

        let font_cache = FontCache::new(document, page_id).context("creating font cache")?;
        let mut state = State::new(config, &font_cache);

        let content = document
            .get_page_content(page_id)
            .context("Unable to get page contents")?;
        let content = Content::decode(&content).context("Unable to decode page content")?;

        for op in content.operations {
            state
                .process_operation(&mut info, &op.operator, &op.operands)
                .with_context(|| anyhow!("While processing operation: {:?}", op))?;
        }
        Ok(info)
    }
}

#[derive(Clone)]
struct GraphicsState {
    transformation_matrix: Matrix<Number>,
}
impl GraphicsState {
    fn new() -> Self {
        Self {
            transformation_matrix: Matrix::identity(), // to verify
        }
    }
}

struct State<'a> {
    config: &'a ExtractConfig,
    font_cache: &'a FontCache<'a>,
    graphics_stack: Vec<GraphicsState>,
    graphics_state: GraphicsState,

    current_point: Vek2,
    current_path: Path,
    paths: Vec<Path>,

    // persistent text state
    character_spacing: Number,
    word_spacing: Number,
    text_horizontal_scaling: Number,
    text_leading: Number,
    text_font: &'a FontInfo<'a>,
    text_font_size: Number,
    // text_rending_mode: TextMode,
    text_rise: Number,
    // text_knockout: bool,

    // Object specific text state
    text_matrix: Matrix<Number>,
    text_line_matrix: Matrix<Number>,
    // text_rendering_matrix: Matrix<Number>,
}

impl<'a> State<'a> {
    fn new(config: &'a ExtractConfig, font_cache: &'a FontCache<'a>) -> Self {
        Self {
            config,
            font_cache,

            graphics_stack: Vec::new(),
            graphics_state: GraphicsState::new(),

            current_point: Vek2::default(),
            current_path: Path::default(),
            paths: Vec::new(),

            character_spacing: 0.0,
            word_spacing: 0.0,
            text_horizontal_scaling: 1.0,
            text_leading: 0.0,
            text_font: font_cache.get_default_font(),
            text_font_size: 1.0, // must be explicitly set
            // text_rending_mode: TextMode::Stroke,
            text_rise: 0.0,

            text_matrix: Matrix::identity(),
            text_line_matrix: Matrix::identity(),
        }
    }

    fn process_operation(
        &mut self,
        extracted_info: &mut ExtractedInfo,
        operator: &str,
        operands: &[Object],
    ) -> Result<()> {
        use Object as O;
        // helper to get the float from either a real or a number
        let t = |o: &Object| match o {
            O::Real(f) => Ok(*f),
            O::Integer(i) => Ok(*i as f32),
            x => bail!("Expected real or integer, found {x:?}"),
        };
        // see Operator Categories (Table 51)
        // take operands as vec of references
        match (operator, operands.iter().collect_vec().as_slice()) {
            //// Graphics state (Table 57)

            // save graphics
            ("q", &[]) => {
                self.graphics_stack.push(self.graphics_state.clone());
            }
            // restore graphics
            ("Q", &[]) => {
                self.graphics_state = self
                    .graphics_stack
                    .pop()
                    .context("Cannot pop graphics stack")?;
            }
            // Concatenate matrix
            ("cm", &[a, b, c, d, e, f]) => {
                // section 8.3.2
                #[rustfmt::skip]
                let matrix = Matrix::new(
                    t(a)?, t(b)?, 0.0,
                    t(c)?, t(d)?, 0.0,
                    t(e)?, t(f)?, 1.0
                );
                // log::debug!("{:?}", matrix);

                // since transformed_point = point * matrix, we need to premultiply.
                // See Note 2 of 8.3.4
                self.graphics_state.transformation_matrix =
                    matrix * self.graphics_state.transformation_matrix;
            }

            ///////// Path Operators

            //// Path constructors (Table 59)
            // start new path
            ("m", &[x, y]) => {
                let point = Vec3 {
                    x: t(x)?,
                    y: t(y)?,
                    z: 1.0,
                } * self.graphics_state.transformation_matrix;
                self.current_point = point.xy();
                if !self.current_path.segments.is_empty() {
                    self.paths.push(std::mem::take(&mut self.current_path));
                }
            }
            // append to current path
            ("l", &[x, y]) => {
                let point = Vec3 {
                    x: t(x)?,
                    y: t(y)?,
                    z: 1.0,
                } * self.graphics_state.transformation_matrix;
                let point = point.xy();
                self.current_path.segments.push((self.current_point, point));
                self.current_point = point;
            }
            // rectangle
            ("re", &[x, y, width, height]) => {
                let x = t(x)?;
                let y = t(y)?;
                let width = t(width)?;
                let height = t(height)?;
                // expand to equivalent
                self.process_operation(extracted_info, "m", &[O::Real(x), O::Real(y)])?;
                self.process_operation(extracted_info, "l", &[O::Real(x + width), O::Real(y)])?;
                self.process_operation(
                    extracted_info,
                    "l",
                    &[O::Real(x + width), O::Real(y + height)],
                )?;
                self.process_operation(extracted_info, "l", &[O::Real(x), O::Real(y + height)])?;
                self.process_operation(extracted_info, "h", &[])?;
            }
            // close
            ("h", &[]) => {
                if let Some(start) = self.current_path.segments.first().map(|s| s.0) {
                    // only do something if not closed
                    if (start - self.current_point).magnitude() > self.config.epsilon {
                        self.current_path.segments.push((self.current_point, start));
                        self.current_point = start;
                    }
                }
            }
            //// Path painting (Table 60)
            // stroke or fill (we dont care which it is)
            ("S", &[]) | ("f", &[]) => {
                if !self.current_path.segments.is_empty() {
                    self.paths.push(std::mem::take(&mut self.current_path));
                }
                extracted_info.paths.append(&mut self.paths);
            }
            // close and stroke
            ("s", &[]) => {
                // expand to equivalent
                self.process_operation(extracted_info, "h", &[])?;
                self.process_operation(extracted_info, "S", &[])?;
            }

            ///////// Text operators

            //// Root operators (Table 107)
            // begin text
            ("BT", &[]) => {
                // text state does not persist between text objects
                self.text_matrix = Matrix::identity();
                self.text_line_matrix = Matrix::identity();
            }
            // end text
            ("ET", &[]) => {
                // no need to do anything since there must be another BT when a new text starts
            }

            //// Text State (Table 105)
            // character spacing
            ("Tc", &[char_space]) => {
                self.character_spacing = t(char_space)?;
            }
            // word spacing
            ("Tw", &[word_space]) => {
                self.word_spacing = t(word_space)?;
            }
            // horizontal scaling
            ("Tz", &[scale]) => {
                self.text_horizontal_scaling = t(scale)? / 100.0;
            }
            // text leading
            ("TL", &[leading]) => {
                self.text_leading = t(leading)?;
            }
            // set font
            ("Tf", &[O::Name(ref name), size]) => {
                self.text_font = self.font_cache.get_by_font_name(name);
                self.text_font_size = t(size)?;
            }

            //// Text Positioning (Table 108)
            // move to next line by offset
            ("Td", &[tx, ty]) => {
                #[rustfmt::skip]
                let matrix = Matrix::new(
                       1.0,     0.0, 0.0,
                       0.0,     1.0, 0.0,
                    t(tx)?,  t(ty)?,  1.0
                );

                self.text_line_matrix = matrix * self.text_line_matrix;
                self.text_matrix = self.text_line_matrix;
            }
            // move to next line by offset and adjust text leading
            ("TD", &[tx, ty]) => {
                let tx = t(tx)?;
                let ty = t(ty)?;
                self.process_operation(extracted_info, "TL", &[O::Real(-ty)])?;
                self.process_operation(extracted_info, "Td", &[O::Real(tx), O::Real(ty)])?;
            }

            // set text matrix
            ("Tm", &[a, b, c, d, e, f]) => {
                // section 8.3.2
                #[rustfmt::skip]
                let matrix = Matrix::new(
                    t(a)?, t(b)?, 0.0,
                    t(c)?, t(d)?, 0.0,
                    t(e)?, t(f)?, 1.0
                );

                self.text_line_matrix = matrix;
                self.text_matrix = matrix;
            }
            // move to next line
            ("T*", &[]) => {
                self.process_operation(
                    extracted_info,
                    "Td",
                    &[O::Real(0.0), O::Real(-self.text_leading)],
                )?;
            }

            //// Text showing (Table 109)
            // show text
            ("Tj", &[O::String(ref string, _string_format)]) => {
                // see 9.4.4 for below formulation
                #[rustfmt::skip]
                let text_rendering_matrix = Matrix::new(
                    self.text_font_size * self.text_horizontal_scaling, 0.0, 0.0,
                    0.0, self.text_font_size, 0.0,
                    0.0, self.text_rise, 1.0,
                ) * self.text_matrix
                    * self.graphics_state.transformation_matrix;

                let pos = Vek::new(0.0, 0.0, 1.0) * text_rendering_matrix;

                let pos = Vek2::new(pos.x, pos.y);

                // log::debug!("{:?}", string);

                match self.text_font.decode(string) {
                    Ok(string) => {
                        extracted_info.text.push(Text { pos, string });
                    }
                    Err(e) => {
                        tracing::warn!("Cannot convert to string {:?}. Error: {:?}", string, e);
                        if let Some((encoding, string_)) =
                            self.config.string_encoding_schemes.iter().find_map(|enc| {
                                Some((
                                    enc,
                                    enc.decode_without_bom_handling_and_without_replacement(
                                        string,
                                    )?,
                                ))
                            })
                        {
                            extracted_info.text.push(Text {
                                pos,
                                string: string_.to_string(),
                            });
                            tracing::warn!(
                                "Successfully decoded {:?} using {}",
                                string,
                                encoding.name()
                            );
                        } else {
                            tracing::warn!(
                                "Failed to decode string. Using the debug string instead"
                            );
                            // Note that this is the debug string of Vec<u8>
                            let string = format!("{:?}", string);

                            extracted_info.text.push(Text { pos, string })
                        }
                    }
                }
            }
            // next line and show
            ("'", &[O::String(..)]) => {
                self.process_operation(extracted_info, "T*", &[])?;
                self.process_operation(extracted_info, "'", operands)?;
            }
            (
                "\"",
                &[aw @ (O::Real(_) | O::Integer(_)), ac @ (O::Real(_) | O::Integer(_)), s @ O::String(..)],
            ) => {
                self.process_operation(extracted_info, "Tw", std::slice::from_ref(aw))?;
                self.process_operation(extracted_info, "Tc", std::slice::from_ref(ac))?;
                self.process_operation(extracted_info, "'", std::slice::from_ref(s))?;
            }
            // draw adjusted
            ("TJ", &[O::Array(ref array)]) => {
                // create a concatinated string instead of trying to mess with glyphs
                let mut string = Vec::new();
                for obj in array {
                    if let O::String(s, _) = obj {
                        string.extend_from_slice(s);
                    }
                }
                self.process_operation(
                    extracted_info,
                    "Tj",
                    &[O::String(string, lopdf::StringFormat::Literal)],
                )?;
            }
            _ => {
                // log::trace!("Skipping operation {operator:?}, {operands:?}");
            }
        }
        Ok(())
    }
}

//! This module parses and sanitizes a single page

use chrono::{Duration, NaiveDateTime, NaiveTime, Timelike};
use itertools::Itertools;
use regex::Regex;
use serde::{Deserialize, Serialize};

use super::{
    layout::assign_regions::{self, AssignedRegions},
    util::Text,
    util::{order_left_right, order_top_bottom},
    Number, ParseConfig,
};

use anyhow::{Context, Result};

type Lines = Vec<Vec<String>>; // top to bottom, left to right

#[serde_with::serde_as]
#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct Meta {
    // from header
    pub page_type: String,
    pub code: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub name: Option<String>,
    pub semester_name: String,
    pub academic_year_start: usize,

    // from footer
    pub page_no: usize,
    #[serde_as(as = "crate::serde_help::SerdeHelp")]
    pub publish_time: NaiveDateTime,
    pub publisher: String,
}
#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum TagType {
    ContainerCode,
    Tag,
}

#[derive(Debug)]
struct HourHeader {
    start: NaiveTime,
    duration: Duration,
    left: Number,
    width: Number,
}

#[serde_with::serde_as]
#[derive(Debug, Serialize, Deserialize, PartialEq, PartialOrd, Ord, Eq)]
pub struct SimpleSession {
    pub day: usize,
    #[serde_as(as = "crate::serde_help::SerdeHelp")]
    pub start: NaiveTime,
    #[serde_as(as = "crate::serde_help::SerdeHelp")]
    pub end: NaiveTime,
    pub r#type: String,
    pub weeks: Vec<usize>,

    // (container_type, code)
    pub containers: Vec<(String, String)>,

    // (tag_class, tag)
    pub tags: Vec<(String, String)>,

    pub skipped: Vec<String>, // we use this to verify that everything was properly parsed.
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct PageData {
    pub meta: Meta,
    pub sessions: Vec<SimpleSession>,
}

impl PageData {
    #[tracing::instrument(skip_all)]
    pub(super) fn parse(assigned_regions: AssignedRegions, config: &ParseConfig) -> Result<Self> {
        let AssignedRegions {
            header,
            footer,
            day_blocks,
            hour_headers,
        } = assigned_regions;
        let meta = Meta::parse(
            get_lines(header, config.line_epsilon),
            get_lines(footer, config.line_epsilon),
            config,
        )?;

        let hour_headers: Vec<_> = hour_headers
            .into_iter()
            .map(|hh| HourHeader::parse(hh, config))
            .collect::<Result<_, _>>()?;

        let sessions: Vec<Vec<_>> = day_blocks
            .into_iter()
            .map(|db| {
                let day = parse_day(db.header, config)?;

                db.content
                    .into_iter()
                    .map(|sb| SimpleSession::parse(sb, day, &hour_headers, config))
                    .collect()
            })
            .collect::<Result<_, _>>()?;

        let sessions = sessions.into_iter().flatten().sorted().collect();

        Ok(Self { meta, sessions })
    }
}

impl Meta {
    /*
     *
     *     SemesterInfo
     *     {Course,Room} timetable - Code, Name
     *     -------------
     *
     *
     *
     *     -------------
     *     page [no], published [date] - Publisher
     */
    fn parse(header: Lines, footer: Lines, config: &ParseConfig) -> Result<Meta> {
        struct Header {
            semester_name: String,
            academic_year_start: usize,
            page_type: String,
            code: String,
            name: Option<String>,
        }
        struct Footer {
            page_no: usize,
            timestamp: NaiveDateTime,
            publisher: String,
        }
        let parse_header = |header: &str, regex: &Regex| {
            let cap = regex.captures(header)?;
            Some(Header {
                semester_name: config.page_semester_formatter.clean(
                cap.name("semester_name")?.as_str()),
                academic_year_start: cap.name("academic_year_start")?.as_str().parse().ok()?,
                page_type: config
                    .page_type_formatter
                    .clean(cap.name("page_type")?.as_str()),
                code: config.page_code_formatter.clean(cap.name("code")?.as_str()),
                name: cap
                    .name("name")
                    .map(|n| config.page_name_formatter.clean(n.as_str())),
            })
        };

        let parse_footer = |footer: &str, regex: &Regex, timestamp_format| {
            let cap = regex.captures(footer)?;
            Some(Footer {
                page_no: cap.name("page_number")?.as_str().parse().ok()?,
                timestamp: NaiveDateTime::parse_from_str(
                    cap.name("timestamp")?.as_str(),
                    timestamp_format,
                )
                .ok()?,
                publisher: cap.name("publisher")?.as_str().to_string(),
            })
        };

        let header = header
            .into_iter()
            .map(|l| l.join(" "))
            .collect::<Vec<_>>()
            .join("\n");
        let Header {
            semester_name,
            academic_year_start,
            page_type,
            code,
            name,
        } = config
            .page_header_formats
            .iter()
            .find_map(|regex| parse_header(&header, regex))
            .with_context(|| format!("no valid format while parsing header: {header:?}"))?;

        let footer = footer
            .into_iter()
            .map(|l| l.join(" "))
            .collect::<Vec<_>>()
            .join("\n");
        let Footer {
            page_no,
            timestamp,
            publisher,
        } = config
            .page_footer_formats
            .iter()
            .find_map(|(regex, timestamp_format)| parse_footer(&footer, regex, timestamp_format))
            .with_context(|| format!("no valid format while parsing footer: {footer:?}"))?;

        Ok(Meta {
            page_type,
            code,
            name,
            page_no,
            publish_time: timestamp,
            publisher,
            semester_name,
            academic_year_start,
        })
    }
}
impl HourHeader {
    fn parse(hour_header: assign_regions::HourHeader, config: &ParseConfig) -> Result<HourHeader> {
        let text = get_lines(hour_header.content, config.line_epsilon)
            .into_iter()
            .map(|l| l.join(" "))
            .collect::<Vec<_>>()
            .join("\n");

        let parse_hour = |hour: &str| {
            let cap = config.page_hour_regex.captures(hour)?;

            NaiveTime::parse_from_str(cap.get(0)?.as_str(), &config.page_hour_format).ok()
        };

        let start = parse_hour(&text).with_context(|| format!("invalid hour: {text:?}"))?;

        Ok(HourHeader {
            start,
            duration: config.page_hour_length,
            left: hour_header.boundary.left(),
            width: hour_header.boundary.width(),
        })
    }
    /// Checks if the x value is in the hour column span
    fn get_time(&self, x: Number) -> Option<NaiveTime> {
        let x = (x - self.left) / self.width;
        if (0.0..1.0).contains(&x) {
            Some(
                self.start
                    + Duration::seconds((self.duration.num_seconds() as Number * x).round() as i64),
            )
        } else {
            None
        }
    }
}

fn parse_day(d: Vec<Text>, config: &ParseConfig) -> Result<usize> {
    let d = get_lines(d, config.line_epsilon)
        .into_iter()
        .map(|l| l.join(""))
        .collect::<Vec<_>>()
        .join("");
    config
        .page_day_strings
        .iter()
        .find(|(s, _)| s.starts_with(&d) && !d.is_empty())
        .map(|(_, d)| *d)
        .with_context(|| format!("invalid day: {d:?}"))
}

impl SimpleSession {
    fn parse(
        session_box: assign_regions::SessionBox,
        day: usize,
        hour_headers: &[HourHeader],
        config: &ParseConfig,
    ) -> Result<SimpleSession> {
        let content = get_lines(session_box.content, config.line_epsilon)
            .into_iter()
            .map(|l| l.join(" "))
            .collect::<Vec<_>>()
            .join(""); // sessions are very weird and like to break lines anywhere

        let start = hour_headers
            .iter()
            .find_map(|h| h.get_time(session_box.boundary.left()))
            .expect("A session box only exists if it lies in the table region")
            - config.page_session_padding_duration;
        let end = hour_headers
            .iter()
            .find_map(|h| h.get_time(session_box.boundary.right()))
            .expect("A session box only exists if it lies in the table region")
            + config.page_session_padding_duration;

        let round_time = |t: NaiveTime| {
            let start = NaiveTime::from_hms_opt(t.hour(), 0, 0).expect("the hour is valid");
            let extra = t - start;

            let count = (extra.num_seconds() * config.page_num_hour_splits as i64 + 1800) / 3600; // rounding division

            let extra = Duration::hours(count) / config.page_num_hour_splits as i32;

            start + extra
        };

        let start = round_time(start);
        let end = round_time(end);

        let parse_week = |s: &str| -> Option<Vec<usize>> {
            let caps = config
                .session_week_formats
                .iter()
                .find_map(|r| r.captures(s))?;

            if let Some((start, end)) = caps.name("start").zip(caps.name("end")) {
                let start = start.as_str().parse::<usize>().ok()?;
                let end = end.as_str().parse::<usize>().ok()?;
                Some((start..=end).collect())
            } else if let Some(week) = caps.name("week") {
                let week = week.as_str().parse::<usize>().ok()?;
                Some(vec![week])
            } else {
                None
            }
        };

        // returns (tags, skipped)
        let parse_tags = |s: &str| -> (Vec<_>, Vec<_>, Vec<_>) {
            let mut remaining = s;
            let mut skipped = Vec::new();
            let mut containers = Vec::new();
            let mut tags = Vec::new();

            while let Some(m) = config.session_tag_format.find(remaining) {
                if m.range.start > 0 {
                    skipped.push(remaining[..m.range.start].to_string());
                }
                remaining = &remaining[m.range.end..];
                for (tag_type, class, tag) in m.value.iter().cloned() {
                    match tag_type {
                        TagType::ContainerCode => containers.push((class, tag)),
                        TagType::Tag => tags.push((class, tag)),
                    }
                }
            }
            // we may have some trailing bit
            if !remaining.is_empty() {
                skipped.push(remaining.to_string())
            }

            (containers, tags, skipped)
        };

        config
            .session_formats
            .iter()
            .find_map(|r| {
                let caps = r.captures(&content)?;

                // 2024, Semester 1: It is now possible that there is no weeks string
                let weeks = if let Some(weeks_str) = caps.name("weeks") {
                    weeks_str
                        .as_str()
                        .split(',')
                        .map(|s| s.trim())
                        .filter(|s| !s.is_empty())
                        .map(parse_week)
                        .collect::<Option<Vec<Vec<usize>>>>()?
                } else {
                    Vec::new()
                };

                let r#type = caps.name("type")?.as_str();
                let r#type = config.session_type_formatter.clean(r#type);

                let weeks = weeks.into_iter().flatten().collect();

                let other = caps.name("other")?.as_str();

                let (containers, tags, skipped) = parse_tags(other);

                Some(SimpleSession {
                    day,
                    start,
                    end,
                    r#type,
                    weeks,
                    containers,
                    tags,
                    skipped,
                })
            })
            .with_context(|| format!("unable to parse session content: {content:?}"))
    }
}

/// This function takes the set a set of text and organizes them into lines
fn get_lines(texts: Vec<Text>, epsilon: Number) -> Lines {
    #[derive(Default)]
    struct Line {
        y: Number,
        texts: Vec<Text>,
    }
    impl Line {
        fn add(&mut self, text: Text) {
            self.texts.push(text);
            self.y = self.texts.iter().map(|t| t.pos.y).sum::<f32>() / self.texts.len() as Number;
        }
    }
    let mut lines: Vec<Line> = Vec::new();

    for text in texts {
        if let Some(line) = lines
            .iter_mut()
            .find(|l| (l.y - text.pos.y).abs() < epsilon)
        {
            line.add(text);
        } else {
            let mut line = Line::default();
            line.add(text);
            lines.push(line);
        }
    }

    lines.sort_by(|a, b| order_top_bottom(&a.y, &b.y));

    lines
        .into_iter()
        .map(|l| l.texts)
        .map(|mut texts| {
            texts.sort_by(|a, b| order_left_right(&a.pos.x, &b.pos.x));
            texts.into_iter().map(|t| t.string).collect()
        })
        .collect()
}

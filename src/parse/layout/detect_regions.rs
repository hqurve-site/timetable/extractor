use std::collections::VecDeque;

use anyhow::{bail, Result};
use serde::{Deserialize, Serialize};

use crate::parse::util::{
    is_above, is_below, order_left_right, order_top_bottom, Path, Rectangle, Vek2,
};

use super::super::{LayoutConfig, Number};

/*
 * Takes a set of paths and produces regions
 */
#[derive(Debug, Serialize, Deserialize)]
pub struct SessionBox {
    pub bounding: Rectangle,
    pub rectangles: Vec<Rectangle>,
}
#[derive(Debug, Serialize, Deserialize)]
pub struct DaySection {
    pub header: Rectangle,
    pub body: Rectangle,
    pub sessions: Vec<SessionBox>,
}
#[derive(Debug, Serialize, Deserialize)]
pub struct DetectedRegions {
    pub header: Number, // max y
    pub footer: Number, // min y

    pub day_sections: Vec<DaySection>,

    pub hour_headers: Vec<Rectangle>,
}

impl SessionBox {
    fn new(rectangle: Rectangle) -> Self {
        Self {
            bounding: rectangle,
            rectangles: vec![rectangle],
        }
    }
    fn add(&mut self, rectangle: Rectangle, config: &LayoutConfig) {
        if rectangle.contains_rect(&self.bounding, config.epsilon) {
            self.bounding = rectangle;
        }
        self.rectangles.push(rectangle);
    }
    fn sutable_to_add(&self, rectangle: &Rectangle, config: &LayoutConfig) -> bool {
        self.bounding.contains_rect(rectangle, config.epsilon)
            || rectangle.contains_rect(&self.bounding, config.epsilon)
    }
}

impl DetectedRegions {
    pub fn parse_from_paths(paths: &[Path], config: &LayoutConfig) -> Result<Self> {
        let mut major_horizontal_lines = Self::get_major_horizontal_lines(paths, config);

        // Important!!
        // We need to remove major lines otherwise they would mess up the rectangle
        // cluster process (ie we get one massive rectangle).
        // However, this is not a problem currently since only sessions use rectangles.

        if major_horizontal_lines.len() < 4 {
            bail!("Too few horizontal lines {}", major_horizontal_lines.len());
        }

        let header = major_horizontal_lines.pop_front().unwrap();
        let footer = major_horizontal_lines.pop_back().unwrap();

        let vertical_sections_header_top = major_horizontal_lines[0];
        let vertical_sections_header_bottom = major_horizontal_lines[1];
        let major_vertical_lines = Self::get_major_vertical_lines(
            paths,
            vertical_sections_header_top,
            vertical_sections_header_bottom,
            config,
        );
        if major_vertical_lines.len() < 3 {
            bail!("Too many vertical lines {}", major_vertical_lines.len());
        }

        let hour_headers = major_vertical_lines
            .windows(2)
            .skip(1)
            .map(|w| {
                Rectangle::from_boundaries(
                    w[0]..w[1],
                    vertical_sections_header_top..vertical_sections_header_bottom,
                )
            })
            .collect();

        let horizontal_section_header_left = major_vertical_lines[0];
        let horizontal_section_header_right = major_vertical_lines[1];
        let horizontal_section_body_right = major_vertical_lines.last().unwrap();

        let mut day_sections: Vec<_> = major_horizontal_lines
            .make_contiguous()
            .windows(2)
            .skip(1)
            .map(|w| {
                let header_rect = Rectangle::from_boundaries(
                    horizontal_section_header_left..horizontal_section_header_right,
                    w[0]..w[1],
                );
                let body_rect = Rectangle::from_boundaries(
                    horizontal_section_header_right..*horizontal_section_body_right,
                    w[0]..w[1],
                );

                DaySection {
                    header: header_rect,
                    body: body_rect,
                    sessions: Vec::new(),
                }
            })
            .collect();

        for sb in Self::cluster_rectangles(Self::get_rectangles(paths, config), config).into_iter()
        {
            if let Some(day_section) = day_sections
                .iter_mut()
                .find(|s| s.body.contains_rect(&sb.bounding, config.epsilon))
            {
                day_section.sessions.push(sb);
            }
        }

        Ok(Self {
            header,
            footer,
            day_sections,
            hour_headers,
        })
    }
    fn get_major_lines<'a, F>(
        paths: &'a [Path],
        config: &'a LayoutConfig,
        filter: F,
    ) -> impl Iterator<Item = StraightLine> + 'a
    where
        F: Fn(&StraightLine) -> bool + 'a,
    {
        let max_length = paths
            .iter()
            .flat_map(|p| &p.segments)
            .cloned()
            .map(|(start, end)| StraightLine { start, end })
            .filter(&filter)
            .map(|l| l.length_squared())
            .reduce(f32::max)
            .unwrap();

        paths
            .iter()
            .flat_map(|p| &p.segments)
            .cloned()
            .map(|(start, end)| StraightLine { start, end })
            .filter(filter)
            .filter(move |l| (l.length_squared() - max_length).abs() < config.epsilon)
    }

    fn get_major_horizontal_lines(paths: &[Path], config: &LayoutConfig) -> VecDeque<Number> {
        let mut lines: Vec<_> = Self::get_major_lines(paths, config, |s| s.is_horizontal(config))
            .map(|l| l.midpoint().y)
            .collect();
        lines.sort_by(order_top_bottom);

        // deduplicate
        lines.into_iter().fold(VecDeque::new(), |mut v, y| {
            if !v.iter().any(|y2| (y2 - y).abs() < config.epsilon) {
                v.push_back(y);
            }
            v
        })
    }

    // Vertical lines may be cut by sessions. So we cannot just take the longest. Instead
    // we take all lines which have a part passing through the header which also have length
    // greater than or equal to the header.
    fn get_major_vertical_lines(
        paths: &[Path],
        header_top: Number,
        header_bottom: Number,
        config: &LayoutConfig,
    ) -> Vec<Number> {
        let length = (header_top - header_bottom).abs();
        let mut lines: Vec<_> = paths
            .iter()
            .flat_map(|p| &p.segments)
            .cloned()
            .map(|(start, end)| StraightLine { start, end })
            .filter(|s| {
                s.is_vertical(config)
                    && s.length() >= length - config.epsilon
                    && (is_above(s.start.y, header_bottom) || is_above(s.end.y, header_bottom))
                    && (is_below(s.start.y, header_top) || is_below(s.end.y, header_top))
            })
            .map(|s| s.midpoint().x)
            .collect();
        lines.sort_by(order_left_right);

        // deduplicate
        lines.into_iter().fold(Vec::new(), |mut v, x| {
            if !v.iter().any(|x2| (x2 - x).abs() < config.epsilon) {
                v.push(x);
            }
            v
        })
    }
    fn get_rectangles(paths: &[Path], config: &LayoutConfig) -> Vec<Rectangle> {
        // For now we only look at paths which contain four segments.
        // A proper solution would be to collect all segments from all the
        // paths and perform some sort of analysis to determine where rectangles are.

        fn try_rectangle(path: &Path, config: &LayoutConfig) -> Option<Rectangle> {
            let points: Vec<_> = path
                .segments
                .iter()
                .cloned()
                .map(|(start, end)| StraightLine { start, end })
                .collect();

            if let [s1, s2, s3, s4] = points.as_slice() {
                if (s1.is_vertical(config)
                    && s2.is_horizontal(config)
                    && s3.is_vertical(config)
                    && s4.is_horizontal(config))
                    || (s1.is_horizontal(config)
                        && s2.is_vertical(config)
                        && s3.is_horizontal(config)
                        && s4.is_vertical(config))
                {
                    Some(Rectangle::from_points(s1.start, s2.end))
                } else {
                    None
                }
            } else {
                None
            }
        }

        paths
            .iter()
            .filter_map(|p| try_rectangle(p, config))
            .collect()
    }

    // returns rectangles which have been clustered
    // O(n^2) lolll
    fn cluster_rectangles(
        mut rectangles: Vec<Rectangle>,
        config: &LayoutConfig,
    ) -> Vec<SessionBox> {
        // we must sort since two smaller rectangles can belong to a larger one.
        rectangles.sort_by(|r1, r2| r2.height().partial_cmp(&r1.height()).unwrap());
        let mut ret: Vec<SessionBox> = Vec::new();

        for rectangle in rectangles {
            if let Some(ref mut session_box) = ret
                .iter_mut()
                .find(|sb| sb.sutable_to_add(&rectangle, config))
                .as_mut()
            {
                session_box.add(rectangle, config);
            } else {
                ret.push(SessionBox::new(rectangle));
            }
        }
        ret
    }
}

struct StraightLine {
    start: Vek2,
    end: Vek2,
}
impl StraightLine {
    fn length(&self) -> Number {
        (self.start - self.end).magnitude()
    }
    fn midpoint(&self) -> Vek2 {
        (self.start + self.end) / 2.0
    }
    fn length_squared(&self) -> Number {
        (self.start - self.end).magnitude_squared()
    }

    fn is_vertical(&self, config: &LayoutConfig) -> bool {
        let Vek2 { x, y } = self.start - self.end;
        (x / y).abs() < config.epsilon
    }
    fn is_horizontal(&self, config: &LayoutConfig) -> bool {
        let Vek2 { x, y } = self.start - self.end;
        (y / x).abs() < config.epsilon
    }
}

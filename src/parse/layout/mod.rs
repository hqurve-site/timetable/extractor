//! Takes the extracted lines and text to produce a set of text clustered into different locations

//! General outline:
//!
//!         Title
//!  -----------------------
//!
//!  -------------------------
//!  |   |Time0 | Time1 | .. |
//!  -------------------------
//!  | D0|      |       |    |
//!  -------------------------
//!  | D1|      |       |    |
//!  -------------------------
//!  | : |      |       |    |
//!  -------------------------
//!
//!  -------------------------
//!        Info (eg timestamp)

use anyhow::Result;
use assign_regions::AssignedRegions;
use detect_regions::DetectedRegions;

use super::{extract::ExtractedInfo, LayoutConfig};

pub mod assign_regions;
pub mod detect_regions;

#[tracing::instrument(skip_all)]
pub fn detect_regions(
    extracted_info: &ExtractedInfo,
    config: &LayoutConfig,
) -> Result<DetectedRegions> {
    DetectedRegions::parse_from_paths(&extracted_info.paths, config)
}
#[tracing::instrument(skip_all)]
pub fn assign_regions(
    extracted_info: ExtractedInfo,
    detected_regions: DetectedRegions,
    config: &LayoutConfig,
) -> Result<AssignedRegions> {
    Ok(AssignedRegions::parse(
        detected_regions,
        extracted_info.text,
        config,
    ))
}

use serde::{Deserialize, Serialize};

use crate::parse::{
    util::{is_above, is_below, Rectangle, Text},
    LayoutConfig,
};

use super::detect_regions::DetectedRegions;

/*
 * Takes regions and text and assigns text to each of the regions
 */

pub type Texts = Vec<Text>;

#[derive(Debug, Serialize, Deserialize)]
pub struct SessionBox {
    pub boundary: Rectangle,
    pub content: Texts,
}
#[derive(Debug, Serialize, Deserialize)]
pub struct HourHeader {
    pub boundary: Rectangle,
    pub content: Texts,
}
#[derive(Debug, Serialize, Deserialize)]
pub struct DayBlock {
    pub header_boundary: Rectangle,
    pub content_boundary: Rectangle,
    pub header: Texts,
    pub content: Vec<SessionBox>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AssignedRegions {
    pub header: Texts,
    pub footer: Texts,

    pub day_blocks: Vec<DayBlock>,
    pub hour_headers: Vec<HourHeader>,
}

impl AssignedRegions {
    pub fn parse(regions: DetectedRegions, texts: Vec<Text>, config: &LayoutConfig) -> Self {
        let mut header = Texts::new();
        let mut footer = Texts::new();

        let mut hour_headers: Vec<_> = regions
            .hour_headers
            .into_iter()
            .map(|h| HourHeader {
                boundary: h,
                content: Texts::new(),
            })
            .collect();
        let mut day_blocks: Vec<_> = regions
            .day_sections
            .into_iter()
            .map(|d| DayBlock {
                header_boundary: d.header,
                header: Texts::new(),
                content_boundary: d.body,
                content: d
                    .sessions
                    .into_iter()
                    .map(|sb| SessionBox {
                        boundary: sb.bounding,
                        content: Texts::new(),
                    })
                    .collect(),
            })
            .collect();

        for text in texts {
            if is_above(text.pos.y, regions.header) {
                header.push(text);
            } else if is_below(text.pos.y, regions.footer) {
                footer.push(text);
            } else if let Some(hour_header) = hour_headers
                .iter_mut()
                .find(|h| h.boundary.contains(&text.pos, config.epsilon))
            {
                hour_header.content.push(text);
            } else if let Some(day_block) = day_blocks
                .iter_mut()
                .find(|db| db.header_boundary.contains(&text.pos, config.epsilon))
            {
                day_block.header.push(text);
            } else if let Some(day_block) = day_blocks
                .iter_mut()
                .find(|db| db.content_boundary.contains(&text.pos, config.epsilon))
            {
                if let Some(session_box) = day_block
                    .content
                    .iter_mut()
                    .find(|sb| sb.boundary.contains(&text.pos, config.epsilon))
                {
                    session_box.content.push(text);
                }
            }
        }

        AssignedRegions {
            header,
            footer,
            day_blocks,
            hour_headers,
        }
    }
}

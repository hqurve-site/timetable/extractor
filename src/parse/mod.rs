//! This module parses all of the downloaded pdfs and stores the results.
//! Each pdf goes through the following stages
//! - extract
//! - detect regions
//! - assign regions
//! - parse
//! - sanitize

use std::collections::HashSet;

use anyhow::{Context, Result};
use extract::ExtractedInfo;
use itertools::Itertools;
use parse::TagType;
use regex::Regex;
use regex_map::{RegexCleaner, RegexMap};
use serde::Deserialize;
use tracing_indicatif::span_ext::IndicatifSpanExt;

use crate::download::Listing;

mod extract;
mod layout;
mod parse;
mod regex_map;
mod util;
pub use parse::PageData;

type Number = f32;

#[derive(Debug, Deserialize)]
pub struct Config {
    // max_parallel: usize,
    extract: ExtractConfig,
    layout: LayoutConfig,
    parse: ParseConfig,
}

/// Contains config for extracting text
#[serde_with::serde_as]
#[derive(Debug, Deserialize)]
struct ExtractConfig {
    // PDF-rs would attempt to decode utf8 and ut16 however, sometimes other schemes are used
    // See https://encoding.spec.whatwg.org/#concept-encoding-get
    #[serde_as(as = "Vec<crate::serde_help::SerdeHelp>")]
    pub string_encoding_schemes: Vec<&'static encoding_rs::Encoding>,
    pub epsilon: Number,
}
/// Contains config for laying out text
#[serde_with::serde_as]
#[derive(Debug, Deserialize)]
struct LayoutConfig {
    pub epsilon: Number,
}

///Contains config for the different aspects of a page
#[serde_with::serde_as]
#[derive(Debug, Deserialize)]
struct ParseConfig {
    pub line_epsilon: Number,

    /* must contain named capturing groups:
     *   - semester_name
     *   - academic_year_start
     *   - page_type
     *   - code
     *   - name
     */
    #[serde_as(as = "Vec<crate::serde_help::SerdeHelp>")]
    pub page_header_formats: Vec<Regex>,
    pub page_semester_formatter: RegexCleaner,
    pub page_type_formatter: RegexCleaner,
    pub page_code_formatter: RegexCleaner,
    pub page_name_formatter: RegexCleaner,

    /* list of (regex, timestamp format)
     * regex must contain named capturing groups:
     *   - page_number
     *   - timestamp
     *   - publisher
     */
    // For timestamp format, see https://docs.rs/chrono/0.4.19/chrono/format/strftime/index.html
    #[serde_as(as = "Vec<(crate::serde_help::SerdeHelp, _)>")]
    pub page_footer_formats: Vec<(Regex, String)>,

    // note that days may be matched using less characters.
    // Strictly, we match a day which it starts with.
    pub page_day_strings: Vec<(String, usize)>,

    // Captures hours and minutes
    #[serde_as(as = "crate::serde_help::SerdeHelp")]
    pub page_hour_regex: Regex,
    // after capture, we parse using the following format.
    // See https://docs.rs/chrono/latest/chrono/format/strftime/index.html
    pub page_hour_format: String,

    // Specifies how many seconds are in an hour slice. what is the length of an hour? hmmmmm?
    // This is just in case they add subhour partitions
    #[serde_as(as = "serde_with::DurationSeconds<i64>")]
    pub page_hour_length: chrono::Duration,
    // Session boxes have a little padding.
    // We add back the size to act as if there is no padding
    #[serde_as(as = "serde_with::DurationSeconds<i64>")]
    pub page_session_padding_duration: chrono::Duration,
    // We round the time of the session to steps of size (1hour / hour_splits)
    pub page_num_hour_splits: usize,

    /*
     * must contain named capturing groups:
     *   - type
     *   - weeks (optional)
     *   - other
     */
    #[serde_as(as = "Vec<crate::serde_help::SerdeHelp>")]
    pub session_formats: Vec<Regex>,
    pub session_type_formatter: RegexCleaner,
    /// contains one of the following
    ///    - start,end
    ///    - week
    /// the regex crate does not support zero-width matchs (positive/negative lookahead/lookbehind)
    #[serde_as(as = "Vec<crate::serde_help::SerdeHelp>")]
    pub session_week_formats: Vec<Regex>,

    /// match a single "tag" or "container".
    /// Maps to one or more sanitized outputs.
    /// regex -> [(tag_type, tag_class, sanitized output)]
    pub session_tag_format: RegexMap<Vec<(TagType, String, String)>>,
}

pub fn run(config: &Config) -> Result<()> {
    // loads list of successful downloads and listing
    let listing: Listing = crate::files::deserialize_sync(crate::files::LISTING_FILE)
        .context("getting listing file")?;
    let successful_downloads: Vec<String> =
        crate::files::deserialize_sync(crate::files::SUCCESSFUL_DOWNLOADS_LIST_FILE)
            .context("getting successful_downloads")?;

    // store as set for easy check
    let successful_downloads: HashSet<String> = successful_downloads.into_iter().collect();
    tracing::info!("loaded listing and successful downloads");

    // try to parse each one of the resources
    let resources = listing
        .resources
        .iter()
        .filter(|resource| {
            let is_staff = resource.r#type.as_ref().is_some_and(|(_, t)| t == "staff");
            let successful = successful_downloads.contains(&resource.id);

            // only parse if it is not staff and successful
            !is_staff && successful
        })
        .collect_vec();

    let pb_span = tracing::info_span!("parsing pdfs");
    pb_span.pb_set_style(&indicatif::ProgressStyle::default_bar());
    pb_span.pb_set_length(resources.len() as u64);
    let pb_span = pb_span.entered();

    let mut results: Vec<_> = resources
        .into_iter()
        .map(|resource| {
            let result = run_single(&resource.id, config, false);
            tracing::Span::current().pb_inc(1);
            (resource.id.clone(), result)
        })
        .collect();

    std::mem::drop(pb_span);
    tracing::info!("parsed all pdfs");

    // ensure sorted
    results.sort_by(|a, b| a.0.cmp(&b.0));

    // now, partition the results into success and failures
    let (successful_parses, failed_parses): (Vec<String>, Vec<(String, String)>) = results
        .into_iter()
        .partition_map(|(rid, result)| match result {
            Ok(()) => itertools::Either::Left(rid),
            Err(e) => itertools::Either::Right((rid, format!("{e:?}"))),
        });

    // save lists to file
    crate::files::serialize_sync(crate::files::SUCCESSFUL_PARSE_LIST_FILE, &successful_parses)
        .context("serializing successful_parses")?;
    crate::files::serialize_sync(crate::files::FAILED_PARSE_LIST_FILE, &failed_parses)
        .context("serializing failed_parses")?;

    Ok(())
}

#[tracing::instrument(skip_all, fields(resource_id=resource_id))]
pub fn run_single(resource_id: &str, config: &Config, save_stages: bool) -> Result<()> {
    // load pdf into memory
    let pdf_path = crate::files::timetable_pdf_pattern(resource_id);

    let pdf_bytes = tracing::info_span!("reading pdf file")
        .in_scope(|| std::fs::read(&pdf_path))
        .with_context(|| format!("reading {pdf_path:?}"))?;

    let document = tracing::info_span!("loading pdf file")
        .in_scope(|| lopdf::Document::load_mem(&pdf_bytes).context("loading pdf"))?;
    std::mem::drop(pdf_bytes);

    let parsed_pages = match save_stages {
        true => parse_single(&document, config, |page, stage| {
            Some(crate::files::timetable_parse_debug_pattern(
                resource_id,
                page,
                stage,
            ))
        })?,
        false => parse_single(&document, config, |_, _| None)?,
    };

    // save parse
    let parse_path = crate::files::timetable_parse_pattern(resource_id);

    tracing::info_span!("saving result")
        .in_scope(|| crate::files::serialize_sync(&parse_path, &parsed_pages))
        .context("saving parsed pages")?;

    Ok(())
}

/// page_stage_save_name takes the page and stage name and produces a filename if the stage is to be saved
#[tracing::instrument(skip_all)]
fn parse_single(
    document: &lopdf::Document,
    config: &Config,
    page_stage_save_name: impl Fn(usize, &str) -> Option<String>,
) -> Result<Vec<PageData>> {
    // parse each page in loop
    let mut parsed_pages = Vec::new();
    for (page, page_id) in document.page_iter().enumerate() {
        let _span = tracing::info_span!("parsing", page = page).entered();
        // extract text and lines
        let extracted_info = ExtractedInfo::extract_from(document, page_id, &config.extract)
            .with_context(|| format!("extracting info, page={page}"))?;
        if let Some(filename) = page_stage_save_name(page, "extract") {
            crate::files::serialize_sync(&filename, &extracted_info)?;
        }

        // perform layout
        let detected_regions = layout::detect_regions(&extracted_info, &config.layout)
            .with_context(|| format!("detect regions, page={page}"))?;
        if let Some(filename) = page_stage_save_name(page, "detectregions") {
            crate::files::serialize_sync(&filename, &detected_regions)?;
        }
        let assigned_regions =
            layout::assign_regions(extracted_info, detected_regions, &config.layout)
                .with_context(|| format!("assign regions, page={page}"))?;
        if let Some(filename) = page_stage_save_name(page, "assignregions") {
            crate::files::serialize_sync(&filename, &assigned_regions)?;
        }

        // perform parse
        let parsed = parse::PageData::parse(assigned_regions, &config.parse)
            .with_context(|| format!("parse, page={page}"))?;
        if let Some(filename) = page_stage_save_name(page, "parse") {
            crate::files::serialize_sync(&filename, &parsed)?;
        }

        parsed_pages.push(parsed)
    }

    Ok(parsed_pages)
}

#[cfg(test)]
mod tests {
    use std::collections::BTreeSet;

    use anyhow::ensure;
    use serde::Serialize;

    use super::*;

    #[derive(Deserialize, Serialize)]
    struct ParsedDocument {
        pages: Vec<PageData>,
    }

    fn test_parse_single(config: &Config, pdf_bytes: &[u8], expected: &[PageData]) -> Result<()> {
        let document = lopdf::Document::load_mem(pdf_bytes).context("loading pdf")?;

        let parsed_pages = parse_single(&document, config, |_, _| None)?;
        let observed = ParsedDocument {
            pages: parsed_pages,
        };

        let skipped: BTreeSet<_> = observed
            .pages
            .iter()
            .flat_map(|page| &page.sessions)
            .flat_map(|session| &session.skipped)
            .collect();
        dbg!(skipped);
        // dbg!(&parsed_pages);
        // println!("{}", toml::to_string_pretty(&observed).unwrap());

        ensure!(observed.pages == expected, "parsed={:?}", observed.pages);

        Ok(())
    }

    #[test]
    fn parse_2022_08_10_test() {
        let config =
            toml::from_str(include_str!("../../test_data/2022-08-10-parse-config.toml")).unwrap();

        let files = [(
            include_bytes!("../../test_data/2022-08-10-m2953.pdf"),
            include_str!("../../test_data/2022-08-10-m2953.toml"),
        )];

        for (i, (pdf_bytes, expected)) in files.into_iter().enumerate() {
            let expected: ParsedDocument = toml::from_str(&expected).unwrap();
            test_parse_single(&config, pdf_bytes, &expected.pages)
                .context(i)
                .unwrap();
        }
    }

    #[test]
    fn parse_2024_10_24_test() {
        let config =
            toml::from_str(include_str!("../../test_data/2024-10-24-parse-config.toml")).unwrap();

        let files = [
            (
                include_bytes!("../../test_data/2024-10-24-m3038.pdf").as_slice(),
                include_str!("../../test_data/2024-10-24-m3038.toml"),
            ),
            (
                include_bytes!("../../test_data/2024-10-24-r3496.pdf"),
                include_str!("../../test_data/2024-10-24-r3496.toml"),
            ),
            (
                include_bytes!("../../test_data/2024-10-24-r3513.pdf"),
                include_str!("../../test_data/2024-10-24-r3513.toml"),
            ),
        ];

        for (i, (pdf_bytes, expected)) in files.into_iter().enumerate() {
            let expected: ParsedDocument = toml::from_str(&expected).unwrap();
            test_parse_single(&config, pdf_bytes, &expected.pages)
                .context(i)
                .unwrap();
        }
    }

    #[test]
    fn enum_toml_serde_test() {
        #[derive(Debug, Serialize, Deserialize, PartialEq)]
        struct Holder {
            enums: Vec<MyEnum>,
        }
        #[derive(Debug, Serialize, Deserialize, PartialEq)]
        enum MyEnum {
            ContainerCode,
            Tag,
        }

        // first test serialize
        let expected = r#"enums = ["ContainerCode", "Tag"]"#;
        let observed = toml::to_string(&Holder {
            enums: vec![MyEnum::ContainerCode, MyEnum::Tag],
        })
        .unwrap();
        assert_eq!(expected, observed.trim());

        // first test deserialize
        let expected = Holder {
            enums: vec![MyEnum::ContainerCode, MyEnum::Tag],
        };
        let observed = toml::from_str(r#"enums=['ContainerCode', 'Tag']"#).unwrap();
        assert_eq!(expected, observed);
    }
}

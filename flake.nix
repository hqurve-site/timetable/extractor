{
  description = "Extract pdfs from mysta.uwi.edu/timetable";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: 
    let
      pkgs = import nixpkgs {inherit system;};
    in rec {
      devShell = with pkgs; mkShell {
        buildInputs = [
          pkg-config
          openssl
          cargo
          rustc
          # rust-bin.nightly.latest.default
          rustfmt
          cargo-play
          cargo-outdated
          clippy
        ];
      };
    }
  );
}
